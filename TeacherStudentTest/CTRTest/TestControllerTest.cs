﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;
using System.Collections.Generic;

namespace TeacherStudentTest.CTRTest
{
    [TestClass]
    public class TestControllerTest
    {
        #region Test Data Members.
        TestController tc;
        Test preparedTest;
        Test preparedTest2;
        Test preparedTest3;
        DateTime deadline = new DateTime(2000, 01, 01, 00, 00, 00);
        #endregion

        //Test initialization. Called implicitly before testing is started.
        [TestInitialize]
        public void SetUp()
        {
            UserController uc = new UserController();
            User user = uc.GetAuthenticatedUser("admin", "admin");
            uc.RememberUser(user);
            preparedTest = new Test();
            preparedTest.TestName = "I don't like tests";

            preparedTest2 = new Test();
            preparedTest2.TestName = "I don't like tests too";

            preparedTest3 = new Test();
            preparedTest3.TestName = String.Empty;

            tc = new TestController();
            preparedTest = tc.InsertTest(preparedTest.TestName);
            preparedTest2 = tc.InsertTest(preparedTest2.TestName);
            if (preparedTest == null || preparedTest2 == null)
            {
                System.Diagnostics.Debug.WriteLine("Missing school class or topic");
            }


            //preparedTest3 will be inserted later. See InsertTest_negative()
        }

        //Test cleaning up. Called implicitly after testing is finished.
        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                tc.DeleteTest(preparedTest.TestId);
                tc.DeleteTest(preparedTest2.TestId);
                tc.DeleteTest(preparedTest3.TestId);
            }
            catch (Exception)
            {
            }

            tc = null;
            preparedTest = null;
            preparedTest2 = null;
            preparedTest3 = null;
        }


        [TestMethod]
        public void InsertTest_positive()
        {
            //Arrangements and getting the actual result.
            Test actualTest = tc.GetTest(preparedTest.TestId);

            //Assertion.
            Assert.IsNotNull(actualTest);
        }

        //Tests with no name are not allowed! This should be unit tested!
        [TestMethod]
        public void InsertTest_negative()
        {
            //Arrangements
            Test actualTest = null;

            //Actuality.
            try
            {
                actualTest = tc.InsertTest(preparedTest3.TestName);
                preparedTest3.TestId = actualTest.TestId;
            }
            catch (Exception)
            {

            }

            //Assertion.
            Assert.IsNull(actualTest);
        }

        [TestMethod]
        public void UpdateTest_positive()
        {
            //Arrangements.
            preparedTest.TestName = "None";

            //Acctuality.
            Test actualTest = tc.UpdateTest(preparedTest.TestId, preparedTest.TestName);

            //Assertion.
            Assert.IsNotNull(actualTest);
            Assert.AreEqual(preparedTest.TestName, actualTest.TestName);
        }

        [TestMethod]
        public void AddQuestionToTest_positive()
        {

        }

        //Tests with no name are not allowed! This should be unit tested!
        [TestMethod]
        public void UpdateTest_negative()
        {
            //Arrangements.
            preparedTest.TestName = "";
            Test actualTest = new Test();

            //Actuality.
            try
            {
                actualTest = tc.UpdateTest(preparedTest.TestId, preparedTest.TestName);
            }
            catch (Exception)
            {
            }

            //Assertion.
            Assert.IsNull(actualTest);
        }

        [TestMethod]
        public void DeleteTest_positive()
        {
            //Arrangements.
            tc.DeleteTest(preparedTest.TestId);
            Test actualTest = null;

            //Actuality.	
            try
            {
                actualTest = tc.GetTest(preparedTest.TestId);
            }
            catch (Exception)
            {
            }

            //Assertion.
            Assert.IsNull(actualTest);
        }

        [TestMethod]
        public void GetSingleTest_positive()
        {
            //Arrangements and Actuality.
            Test actualTest = tc.GetTest(preparedTest.TestId);

            //Assertion.
            Assert.AreEqual(preparedTest.TestId, actualTest.TestId);
        }

        [TestMethod]
        public void GetAllTests_positive()
        {
            //Arrangements and Actuality.
            List<Test> actualTests = tc.GetAllTests();

            //Assertion.
            Assert.IsTrue(actualTests.Count >= 2);
        }

        [TestMethod]
        public void SendTestToClass_positive()
        {
            TestController tc = new TestController();
            SchoolClass sc = new SchoolClass();
            sc.SchoolClassId = 1;
            Test t = tc.InsertTest("testName");
            Topic topic = new Topic();
            topic.TopicId = 1;
            Assert.IsNotNull(tc.SendToClass(t, sc, topic, DateTime.Now.AddDays(7)));
            tc.DeleteTest(t.TestId);
            
        }


    }
}
