﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentTest.CTRTest
{
    [TestClass]
    class AnnouncementControllerTest
    {
        [TestMethod]
        public void CreateAnnouncement_negative()
        {
            Announcement announce = null;
            var an = new AnnouncementController();
            try
            {
                announce = an.InsertAnnouncement("Test message announcement", -1);
            }
            catch (Exception)
            {
                //Message will be null
            }
            Assert.IsNull(announce);
        }
        [TestMethod]
        public void CreateAnnouncement_positive()
        {
            var an = new AnnouncementController();
            Announcement announce = an.InsertAnnouncement("Test messaage announcement", 1);
            Assert.IsNotNull(announce);
            //cleanup
        }
    }
}
