﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentTest.CTRTest
{
    [TestClass]
    public class QuestionControllerTest
    {
        #region Test Data Members.
        QuestionController qc;
        Question preparedQuestion;
        Question preparedQuestion2;
        Question preparedQuestion3;
        #endregion

        //Test initialization. Called implicitly before testing is started.
        [TestInitialize]
        public void SetUp()
        {
            preparedQuestion = new Question();
            preparedQuestion.QuestionText = "What do you want to ask?";

            preparedQuestion2 = new Question();
            preparedQuestion2.QuestionText = String.Empty;

            preparedQuestion3 = new Question();
            preparedQuestion3.QuestionText = "      ";

            qc = new QuestionController();
            preparedQuestion = qc.InsertQuestion(preparedQuestion.QuestionText);
            preparedQuestion2 = qc.InsertQuestion(preparedQuestion2.QuestionText);
            preparedQuestion3 = qc.InsertQuestion(preparedQuestion3.QuestionText);
        }

        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                qc.DeleteQuestion(preparedQuestion.QuestionId);
            }
            catch (Exception)
            {
                Assert.Fail("Something went wrong when deleting question!");
            }

            qc = null;
            preparedQuestion = null;
            preparedQuestion2 = null;
            preparedQuestion3 = null;
        }

        [TestMethod]
        public void GetSingleQuestion_positive()
        {
            //Arrangements and actuality.
            Question actualQuestion = qc.GetSingleQuestion(preparedQuestion.QuestionId);

            //Assertion.
            Assert.AreEqual(preparedQuestion.QuestionId, actualQuestion.QuestionId);
        }

        [TestMethod]
        public void InsertQuestion_positive()
        {
            //Arrangements and Actuality.
            Question actualQuestion = qc.GetSingleQuestion(preparedQuestion.QuestionId);

            //Assertion.
            Assert.IsNotNull(actualQuestion);

        }

        [TestMethod]
        public void InsertQuestion_negative()
        {
            //Actuality.
            try
            {
                preparedQuestion2 = qc.InsertQuestion(preparedQuestion2.QuestionText);
                preparedQuestion3 = qc.InsertQuestion(preparedQuestion3.QuestionText);
            }
            catch (Exception)
            {

            }

            //Assertion.
            Assert.IsNull(preparedQuestion2);
            Assert.IsNull(preparedQuestion3);
        }

        [TestMethod]
        public void DeleteQuestion_positive()
        {
            //Arrangements.
            qc.DeleteQuestion(preparedQuestion.QuestionId);
            Question actualQuestion = null;

            //Actuality.
            actualQuestion = qc.GetSingleQuestion(preparedQuestion.QuestionId);

            //Assertion.
            Assert.IsNull(actualQuestion);
        }

    }
}
