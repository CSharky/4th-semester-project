﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentTest.CTRTest
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void CreateUserStudent_negative()
        {
            Student user = null;
            var uc = new StudentController();
            try
            {
                user = uc.CreateStudent("laszlo", "muresan", "****@gmail.com", -100, "userfrom test ", "password");
            }
            catch (Exception)
            {
                //USER WILL BE NULL
            }
            Assert.IsNull(user);
        }
        [TestMethod]
        public void CreateUserStudent_positive()
        {
            var uc = new StudentController();
            var us = new UserController();
            Student user = uc.CreateStudent("laszlo", "muresan", "****@gmail.com", 1, "userfrom deleted", "password");
            Assert.IsNotNull(user);
            //cleanup manual
            us.DeleteUser("userfrom deleted");

        }

        [TestMethod]
        public void CreateUserTeacher_negative()
        {
            Teacher teacher = null;
            var tc = new TeacherController();
            try
            {
                teacher = tc.CreateTeacher("laszlo", "muresan", "****@gmail.com", "userfrom test ", "password");
            }
            catch (Exception)
            {
                //USER WILL BE NULL
            }
            Assert.IsNull(teacher);
        }
        [TestMethod]
        public void CreateUserTeacher_positive()
        {
            var tc = new TeacherController();
            var uc = new UserController();
            Teacher teacher = tc.CreateTeacher("laszlo", "muresan", "****@gmail.com", "userfrom deleted", "password");
            Assert.IsNotNull(teacher);
            //cleanup manual
            uc.DeleteUser("userfrom deleted");

        }
    }
}
