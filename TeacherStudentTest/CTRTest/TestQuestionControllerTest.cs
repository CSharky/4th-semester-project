﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentTest.CTRTest
{
    [TestClass]
    public class TestQuestionControllerTest
    {
        #region Test Data Members.
        //Preconditions.
        DateTime deadline = new DateTime(2000, 01, 01, 00, 00, 00);

        TestController tc;
        Test preparedTest;

        QuestionController qc;
        Question preparedQuestion;

        //The actual stuff that must be tested.
        TestQuestionController tqc;
        TestQuestion preparedTestQuestion;
        #endregion

        [TestInitialize]
        public void SetUp()
        {
            UserController uc = new UserController();
            User user = uc.GetAuthenticatedUser("admin", "admin");
            uc.RememberUser(user);
            preparedTest = new Test();
            preparedTest.TestName = "Pffffff";
            tc = new TestController();
            preparedTest.TestId = tc.InsertTest(preparedTest.TestName).TestId;

            preparedQuestion = new Question();
            preparedQuestion.QuestionText = "No no!";
            qc = new QuestionController();
            preparedQuestion.QuestionId = qc.InsertQuestion(preparedQuestion.QuestionText).QuestionId;

            preparedTestQuestion = new TestQuestion();
            preparedTestQuestion.TestId = preparedTest.TestId;
            preparedTestQuestion.QuestionId = preparedQuestion.QuestionId;
            tqc = new TestQuestionController();
            preparedTestQuestion.TestQuestionId = tqc.InsertTestQuestion(preparedTestQuestion.TestId,
                            preparedTestQuestion.QuestionId).TestQuestionId;
        }

        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                //tqc.DeleteTestQuestion(preparedTestQuestion.TestQuestionId);
                qc.DeleteQuestion(preparedQuestion.QuestionId);
                tc.DeleteTest(preparedTest.TestId);
            }
            catch (Exception)
            {
            }

            tqc = null;
            qc = null;
            tc = null;

            preparedTestQuestion = null;
            preparedQuestion = null;
            preparedTest = null;

        }

        [TestMethod]
        public void GetSingleTestQuestion_positive()
        {
            //Arrangements and actuality.
            TestQuestion actualTestQuestion = tqc.GetSingleTestQuestion(preparedTestQuestion.TestQuestionId);

            //Assertion.
            Assert.AreEqual(preparedTestQuestion.TestQuestionId, actualTestQuestion.TestQuestionId);
        }

        [TestMethod]
        public void InsertTestQuestion_positive()
        {
            //Arrangements and Actuality.
            TestQuestion actualTestQuestion = tqc.GetSingleTestQuestion(preparedTestQuestion.TestQuestionId);

            //Assertion.
            Assert.IsNotNull(actualTestQuestion);
        }

        [TestMethod]
        public void DeleteTestQuestion_positive()
        {
            //Arrangements.
            tqc.DeleteTestQuestion(preparedTestQuestion.TestQuestionId);
            TestQuestion actualTestQuestion = null;

            //Actuality.
            try
            {
                actualTestQuestion = tqc.GetSingleTestQuestion(preparedTestQuestion.TestQuestionId);
            }
            catch (Exception)
            {
            }

            //Assertion.
            Assert.IsNull(actualTestQuestion);
        }

        [TestMethod]
        public void DeleteTestQuestionFromDBAndTests()
        {
            //Arrangements.
            tqc.DeleteTestQuestionFromDBAndTests(preparedTestQuestion.TestQuestionId);
            TestQuestion actualTestQuestion = null;
            Question actualQuestion = null;

            //Actuality.
            actualTestQuestion = tqc.GetSingleTestQuestion(preparedTestQuestion.TestQuestionId);
            actualQuestion = qc.GetSingleQuestion(preparedQuestion.QuestionId);

            //Assertion.
            Assert.IsNull(actualTestQuestion);
            Assert.IsNull(actualQuestion);
        }
    }
}
