GO
CREATE TABLE [4thsemester].[Users] (
    [UserId]   INT           IDENTITY (1, 1) NOT NULL,
    [Username] VARCHAR (100) NULL,
    [Password] VARCHAR (100) NULL,
    [Salt]     VARCHAR (28)  NULL,
    [Rights]   VARCHAR (25)  NULL,
    CONSTRAINT [pk_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
);
GO
CREATE TABLE [4thsemester].[Teacher] (
    [TeacherId] INT          IDENTITY (1, 1) NOT NULL,
    [Fname]     VARCHAR (24) NOT NULL,
    [Lname]     VARCHAR (24) NOT NULL,
    [Email]     VARCHAR (24) NULL,
    [UserId]    INT          NOT NULL,
    CONSTRAINT [pk_Teacher] PRIMARY KEY CLUSTERED ([TeacherId] ASC),
    CONSTRAINT [fk_User_Teacher] FOREIGN KEY ([UserId]) REFERENCES [4thsemester].[Users] ([UserId]) ON DELETE CASCADE
);
GO
CREATE TABLE [4thsemester].[Topic] (
    [TopicId]   INT          IDENTITY (1, 1) NOT NULL,
    [TopicName] VARCHAR (24) NOT NULL,
    CONSTRAINT [pk_Topic] PRIMARY KEY CLUSTERED ([TopicId] ASC)
);
GO
CREATE TABLE [4thsemester].[SchoolClass] (
    [SchoolClassId]   INT          IDENTITY (1, 1) NOT NULL,
    [SchoolClassName] VARCHAR (24) NOT NULL,
    CONSTRAINT [pk_SchoolClass] PRIMARY KEY CLUSTERED ([SchoolClassId] ASC)
);
GO
CREATE TABLE [4thsemester].[Question] (
    [QuestionId]   INT           IDENTITY (1, 1) NOT NULL,
    [QuestionText] VARCHAR (150) NOT NULL,
    CONSTRAINT [pk_Question] PRIMARY KEY CLUSTERED ([QuestionId] ASC)
);
GO
CREATE TABLE [4thsemester].[Teaches] (
    [TeachesId]     INT IDENTITY (1, 1) NOT NULL,
    [TeacherId]     INT NOT NULL,
    [SchoolClassId] INT NOT NULL,
    [TopicId]       INT NOT NULL,
    CONSTRAINT [pk_Teaches] PRIMARY KEY CLUSTERED ([TeachesId] ASC),
    CONSTRAINT [fk1_Teacher] FOREIGN KEY ([TeacherId]) REFERENCES [4thsemester].[Teacher] ([TeacherId]) ON DELETE CASCADE,
    CONSTRAINT [fk2_SchoolClass] FOREIGN KEY ([SchoolClassId]) REFERENCES [4thsemester].[SchoolClass] ([SchoolClassId]) ON DELETE CASCADE,
    CONSTRAINT [fk3_Topic] FOREIGN KEY ([TopicId]) REFERENCES [4thsemester].[Topic] ([TopicId]) ON DELETE CASCADE
);
GO
CREATE TABLE [4thsemester].[Test] (
    [TestId]    INT          IDENTITY (1, 1) NOT NULL,
    [TestName]  VARCHAR (24) NOT NULL,  
    CONSTRAINT [pk_Test] PRIMARY KEY CLUSTERED ([TestId] ASC)
);
GO
CREATE TABLE [4thsemester].[ClassTest] (
    [ClassTestId]   INT IDENTITY (1, 1) NOT NULL,
    [TestId]        INT NOT NULL,
    [TeachesId] INT NOT NULL,
	[Deadline]  DATETIME     NOT NULL,
    PRIMARY KEY CLUSTERED ([ClassTestId] ASC),
    FOREIGN KEY ([TestId]) REFERENCES [4thsemester].[Test] ([TestId]) ON DELETE CASCADE,
    FOREIGN KEY ([TeachesId]) REFERENCES [4thsemester].[Teaches] ([TeachesId]) ON DELETE CASCADE
);
GO
CREATE TABLE [4thsemester].[Student] (
    [StudentId]     INT          IDENTITY (1, 1) NOT NULL,
    [Fname]         VARCHAR (24) NOT NULL,
    [Lname]         VARCHAR (24) NOT NULL,
    [Email]         VARCHAR (24) NULL,
    [Score]         INT          NOT NULL,
    [SchoolClassId] INT          DEFAULT ((0)) NOT NULL,
    [UserId]        INT          NOT NULL,
    [Picture64] VARCHAR(100)	NULL,
    CONSTRAINT [pk_Student] PRIMARY KEY CLUSTERED ([StudentId] ASC),
    CONSTRAINT [fk_User_Student] FOREIGN KEY ([UserId]) REFERENCES [4thsemester].[Users] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [fk_Student_SchoolClass] FOREIGN KEY ([SchoolClassId]) REFERENCES [4thsemester].[SchoolClass] ([SchoolClassId]) ON DELETE CASCADE
);
GO
CREATE TABLE [4thsemester].[Answer] (
    [AnswerId]       INT           IDENTITY (1, 1) NOT NULL,
    [TestQuestionId] INT           NOT NULL,
    [StudentId]      INT           NOT NULL,
    [AnswerSent]     DATETIME      NOT NULL,
    [AnswerText]     VARCHAR (150) NULL,
    [IsCorrect]      BIT           NULL,
    CONSTRAINT [pk_Answer] PRIMARY KEY CLUSTERED ([AnswerId] ASC),
    CONSTRAINT [fk1_TestQuestion] FOREIGN KEY ([TestQuestionId]) REFERENCES [4thsemester].[TestQuestion] ([TestQuestionId]) ON DELETE CASCADE,
    CONSTRAINT [fk2_Student] FOREIGN KEY ([StudentId]) REFERENCES [4thsemester].[Student] ([StudentId]) ON DELETE CASCADE
);
GO
CREATE TABLE [4thsemester].[TestQuestion] (
    [TestQuestionId] INT IDENTITY (1, 1) NOT NULL,
    [TestId]         INT NOT NULL,
    [QuestionId]     INT NOT NULL,
    CONSTRAINT [pk_TestQuestion] PRIMARY KEY CLUSTERED ([TestQuestionId] ASC),
    CONSTRAINT [fk1_Test] FOREIGN KEY ([TestId]) REFERENCES [4thsemester].[Test] ([TestId]) ON DELETE CASCADE,
    CONSTRAINT [fk2_Question] FOREIGN KEY ([QuestionId]) REFERENCES [4thsemester].[Question] ([QuestionId]) ON DELETE CASCADE
);

