﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.GUI
{
		public partial class LecturerForm
		{

				private void LoadVerifyAnswersTab()
				{
						//The id of the authenticated teacher is used, so that all her/his classes will be fetched.
						int teacherId = new TeacherController().GetTeacher(Global.AUTHENTICATED_USER.UserId).TeacherId;
						IList<SchoolClass> classes = new TeachesController().GetClassesForTeacher(teacherId);

						//Set up of the combo box for classes.
						class_comboBox2.DataSource = null;
						class_comboBox2.AutoCompleteSource = AutoCompleteSource.CustomSource;
						class_comboBox2.DataSource = classes;
						class_comboBox2.DisplayMember = "DisplayName";						
				}

				private void class_comboBox2_SelectedValueChanged(object sender, EventArgs e)
				{
						if (class_comboBox2.SelectedItem != null)
						{	
								//The schoolClassId is needed to get the topics for a particular class.
								int schoolClassId = ((SchoolClass)class_comboBox2.SelectedItem).SchoolClassId;
								IList<Topic> topics = new TeachesController().GetTopicsForClass(schoolClassId);

								//Set up of the combo box for subjects.
								subject_comboBox2.DataSource = null;
								subject_comboBox2.AutoCompleteSource = AutoCompleteSource.CustomSource;	
								subject_comboBox2.DataSource = topics;
								subject_comboBox2.DisplayMember = "DisplayName";
						}
				}

				private void subject_comboBox2_SelectedValueChanged(object sender, EventArgs e)
				{
						if (subject_comboBox2.SelectedItem != null)
						{
								//The schoolClassId and the topicId are needed to get the tests for a particular class on a particular topic.
								int schoolClassId = ((SchoolClass)class_comboBox2.SelectedItem).SchoolClassId;
								int topicId = ((Topic)subject_comboBox2.SelectedItem).TopicId;
								IList<Test> tests = new TestController().GetTestsForClassAndSubject(schoolClassId, topicId);

								//Set up of the combo box for tests.
								test_comboBox2.DataSource = null;
								test_comboBox2.AutoCompleteSource = AutoCompleteSource.CustomSource;
								test_comboBox2.DataSource = tests;
								test_comboBox2.DisplayMember = "DisplayName";			
						}									
				}

				private void test_comboBox2_SelectedValueChanged(object sender, EventArgs e)
				{
						if (test_comboBox2.SelectedItem != null)
						{
								//The testId is need to get all the questions of a particular test.
								int testId = ((Test)test_comboBox2.SelectedItem).TestId;
								IList<Question> questions = new TestController().GetTestQuestionsForDataset(testId);

								//Set up of combo box for questions.
								question_comboBox2.DataSource = null;
								question_comboBox2.AutoCompleteSource = AutoCompleteSource.CustomSource;
								question_comboBox2.DataSource = questions;
								question_comboBox2.DisplayMember = "DisplayName";	
						}
				}

				private void question_comboBox2_SelectedValueChanged(object sender, EventArgs e)
				{
						UpdateVerificationGrid();
				}

				private void UpdateVerificationGrid()
				{
						if(test_comboBox2.SelectedItem != null && question_comboBox2.SelectedItem != null) {

								int testId = ((Test)test_comboBox2.SelectedItem).TestId;
								int questionId = ((Question)question_comboBox2.SelectedItem).QuestionId;
								IList<StudentAnswer> answers = new AnswerController().GetAnswersForDataset(testId, questionId);

								verification_grid.DataSource = null;
								verification_grid.AutoGenerateColumns = true;
								verification_grid.DataSource = answers;

								#region Cell autoresize and wrap mode settings.
								verification_grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
								verification_grid.Columns["AnswerText"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;						

								verification_grid.Columns["AnswerId"].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
								verification_grid.Columns["AnswerId"].Width = 45;
								verification_grid.Columns["FirstName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
								verification_grid.Columns["LastName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
								verification_grid.Columns["AnswerText"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
								verification_grid.Columns["IsCorrect"].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
								verification_grid.Columns["IsCorrect"].Width = 30;
								verification_grid.Columns["Points"].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
								verification_grid.Columns["Points"].Width = 45;
								#endregion
								
								#region Header values.
								verification_grid.Columns["AnswerId"].HeaderText = "ID";
								verification_grid.Columns["FirstName"].HeaderText = "First Name";
								verification_grid.Columns["LastName"].HeaderText = "Last Name";
								verification_grid.Columns["AnswerText"].HeaderText = "Answer";
								verification_grid.Columns["IsCorrect"].HeaderText = "✓";
								verification_grid.Columns["Points"].HeaderText = "Points";
								#endregion

								#region Setting some of the columns as read-only
								verification_grid.Columns["AnswerId"].ReadOnly = true;
								verification_grid.Columns["StudentId"].ReadOnly = true;
								verification_grid.Columns["FirstName"].ReadOnly = true;
								verification_grid.Columns["LastName"].ReadOnly = true;
								verification_grid.Columns["AnswerText"].ReadOnly = true;
								#endregion

								#region Hidden columns.
								verification_grid.Columns["StudentId"].Visible = false;
								#endregion
						}
				}

				private void verify_btn_Click(object sender, EventArgs e)
				{
						AnswerController ac = new AnswerController();
						StudentController sc = new StudentController();

						foreach (DataGridViewRow r in verification_grid.Rows)
						{
								int answerId = (int)verification_grid[verification_grid.Columns["AnswerId"].Index, r.Index].Value;
								bool isCorrect = (bool)verification_grid[verification_grid.Columns["IsCorrect"].Index, r.Index].Value;
								ac.SetIsCorrect(answerId, isCorrect);
								
								int studentId = (int)verification_grid[verification_grid.Columns["StudentId"].Index, r.Index].Value;								
								int points = (int)verification_grid[verification_grid.Columns["Points"].Index, r.Index].Value;								
								sc.AddPoints(studentId, points);
								}
						UpdateVerificationGrid();
				}
		}
}
