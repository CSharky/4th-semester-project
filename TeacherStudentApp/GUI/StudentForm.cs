﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.GUI
{
    public partial class StudentForm : Form
    {

        public StudentForm()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            StudentController sc = new StudentController();
            sc.CreateStudent("laszlo", "muresan", "ellis@gmail.com", 1, "user", "password");
        }

        private void class_combo_box_SelectedValueChanged(object sender, EventArgs e)
        {
            TopicController tc = new TopicController();
            TeachesController tcs = new TeachesController();
            subject_combo_box.DataSource = null;
            subject_combo_box.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SchoolClass sc = (SchoolClass)class_combo_box.SelectedItem;
            subject_combo_box.DataSource = tcs.GetTopicsForClass(sc.SchoolClassId);
            subject_combo_box.DisplayMember = "DisplayName";
            //updateGrid();
        }

        //private IList<Test> SortTestDropDown()
        //{
        //    using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
        //    {
        //        //IList<Test> tests = new List<Test>();
        //        //Topic topic = GetSubject();
        //        //if (topic != null)
        //        //{
        //        //    var teaches = db.Teaches.Where(t => t.TopicId == topic.TopicId && t.SchoolClassId == GetClass().SchoolClassId);
        //        //    if (teaches != null)
        //        //    {
        //        //        foreach (Teach a in teaches)
        //        //        {
        //        //            var test = db.Tests.Where(teach => teach.TeachesId == a.TeachesId);
        //        //            tests = test.ToList();
        //        //        }
        //        //    }
        //        //}
        //        ////TODO Find an easier way to solve this


        //        //return tests;
        //        return null;
        //    }
        //}

        //private IList<Topic> SortSubjectDropDown()
        //{
        //    using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
        //    {
        //        IList<Topic> topics = new List<Topic>();
        //        SchoolClass sc = GetClass();
        //        var teaches = db.Teaches.Where(t => t.SchoolClassId == sc.SchoolClassId);
        //        //TODO Find an easier way to solve this
        //        foreach (Teach a in teaches)
        //            topics.Add(a.Topic);
        //        return topics;
        //    }
        //}

        private Topic GetSubject()
        {
            Topic t = (Topic)subject_combo_box.SelectedItem;
            return t;
        }

        private SchoolClass GetClass()
        {
            SchoolClass sc = (SchoolClass)class_combo_box.SelectedItem;
            return sc;
        }

        private Test GetTest()
        {
            Test test = (Test)test_combo_box.SelectedItem;
            return test;
        }

        private Teach GetTeaches()
        {
            Topic topic = GetSubject();
            SchoolClass schoolClass = GetClass();
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                if (topic != null)
                {
                    Teach teach = db.Teaches.SingleOrDefault(t => t.TopicId == topic.TopicId && t.SchoolClassId == schoolClass.SchoolClassId);
                    return teach;
                }
                else
                    return null;
            }
        }

        private ClassTest GetClassTest()
        {
            Test test = GetTest();
            Teach teaches = GetTeaches();
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                if (test != null && teaches != null)
                {
                    ClassTest ct = db.ClassTests.SingleOrDefault(cts => cts.TestId == test.TestId && cts.TeachesId == teaches.TeachesId);
                    return ct;
                }
                else
                    return null;
            }
        }
        private bool IsTestExpiredOrFull()
        {
            ClassTest ct = GetClassTest();
            bool expired = false;
            if (ct != null)
            {
                TimeSpan remaining = ct.Deadline.Subtract(DateTime.Now);
                if (remaining.Days < 0 || remaining.Hours < 0 || remaining.Minutes < 0)
                    expired = true;
                if (!expired)
                {
                    foreach (DataGridViewRow row in questions_grid.Rows)
                    {
                        QuestionAnswer qa = (QuestionAnswer)row.DataBoundItem;
                        if (qa.AnswerText == null || qa.AnswerText.Trim() == "")
                        {
                            expired = false;
                            break;
                        }
                        else
                        {
                            expired = true;
                        }
                    }
                }
            }
            return expired;
        }
        private void UpdateDeadline()
        {
            ClassTest ct = GetClassTest();
            if (ct != null)
            {
                TimeSpan remaining = ct.Deadline.Subtract(DateTime.Now);
                deadline_label.Text = "Days: " + (remaining.Days < 0 ? 0 : remaining.Days) + " Hours: " + (remaining.Hours < 0 ? 0 : remaining.Hours) + " Minutes: " + (remaining.Minutes < 0 ? 0 : remaining.Minutes);
            }
        }

        private void StudentForm_Load(object sender, EventArgs e)
        {
            SClassController scc = new SClassController();
            class_combo_box.AutoCompleteSource = AutoCompleteSource.CustomSource;
            class_combo_box.DataSource = scc.GetDropDownClassesForStudent();
            class_combo_box.DisplayMember = "DisplayName";

            TestController tc = new TestController();
            AnnouncementController ac = new AnnouncementController();

            StudentController sc = new StudentController();
            Student student = sc.GetStudent(Global.AUTHENTICATED_USER.UserId);

            announcement_list.Items.AddRange(ac.GetAllAnnouncementsForClass(student.SchoolClassId).ToArray());

            class_list.Items.AddRange(tc.GetAllTestsForClass(student.SchoolClassId).ToArray());
            updateRankGrid();


            //for profile picture
            string base64Image = sc.GetPicture(sc.GetStudent(Global.AUTHENTICATED_USER.UserId).StudentId);
            if (base64Image != null && base64Image.Length > 2)
                profile_picture_box.Image = Base64ToImage(base64Image);
        }

        private void subject_combo_box_SelectedValueChanged(object sender, EventArgs e)
        {
            TestController tc = new TestController();
            test_combo_box.DataSource = null;
            test_combo_box.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SchoolClass sc = GetClass();
            Topic to = GetSubject();
            if (to != null)
                test_combo_box.DataSource = tc.GetTestsForClassAndSubject(sc.SchoolClassId, to.TopicId);
            else
                test_combo_box.DataSource = tc.GetTestsForClass(sc.SchoolClassId);
            test_combo_box.DisplayMember = "DisplayName";
            //updateGrid();
        }

        private void test_combo_box_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateDeadline();
            updateGrid();
        }

        private void updateGrid()
        {

            questions_grid.DataSource = null;
            ComboBox cb = test_combo_box;
            Test test = (Test)cb.SelectedItem;

            if (test != null)
            {
                TestController tc = new TestController();
                questions_grid.AutoGenerateColumns = true;
                IList<QuestionAnswer> list = tc.GetQuestionAnswerForDataset(test.TestId);
                questions_grid.DataSource = list;

                //Cell autoresize and wrap mode settings.
                questions_grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                questions_grid.Columns["AnswerText"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                questions_grid.Columns["questionText"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                questions_grid.Columns["TestQuestionId"].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                questions_grid.Columns["TestQuestionId"].Width = 45;
                questions_grid.Columns["questionText"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                questions_grid.Columns["AnswerText"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                //Header values.
                questions_grid.Columns["TestQuestionId"].HeaderText = "ID";
                questions_grid.Columns["questionText"].HeaderText = "Question";
                questions_grid.Columns["AnswerText"].HeaderText = "Answer";
            }
            foreach (DataGridViewRow row in questions_grid.Rows)
            {
                QuestionAnswer qa = (QuestionAnswer)row.DataBoundItem;
                if (qa.AnswerText != null)
                {
                    string ans = qa.AnswerText.ToUpper().Trim();
                    if (ans != "NOT ANSWERED" && ans != "")
                        row.ReadOnly = true;
                }
            }
            if (IsTestExpiredOrFull())
            {
                questions_grid.Enabled = false;
                send_answer_btn.Enabled = false;
            }
            else
            {
                questions_grid.Enabled = true;
                send_answer_btn.Enabled = true;
            }
        }

        private IList<string> GetAnswersFromTable()
        {
            IList<string> answers = new List<string>();

            foreach (DataGridViewRow r in questions_grid.Rows)
            {

                if (questions_grid[questions_grid.Columns["AnswerText"].Index, r.Index].Value != null && questions_grid[questions_grid.Columns["AnswerText"].Index, r.Index].Value.ToString().Trim() != "")
                {
                    answers.Add(questions_grid[questions_grid.Columns["AnswerText"].Index, r.Index].Value.ToString());
                }
                else
                {
                    answers.Add("");
                }
            }
            return answers;
        }

        private IList<int> GetTestQuestionId()
        {
            IList<int> testQuestionIds = new List<int>();
            int testId = GetTest().TestId;
            foreach (DataGridViewRow r in questions_grid.Rows)
            {
                int testQuestionId = Convert.ToInt32(questions_grid[questions_grid.Columns["TestQuestionID"].Index, r.Index].Value);
                testQuestionIds.Add(testQuestionId);
            }
            return testQuestionIds;
        }

        private void send_answer_btn_Click(object sender, EventArgs e)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                AnswerController ac = new AnswerController();
                StudentController sc = new StudentController();
                IList<int> testQuestionIds = GetTestQuestionId();
                IList<string> answers = GetAnswersFromTable();
                Student student = sc.GetStudent(Global.AUTHENTICATED_USER.UserId);
                if (student != null)
                {
                    for (int i = 0; i < testQuestionIds.Count; i++)
                    {
                        if (ac.GetSingleAnswer(testQuestionIds[i], student.StudentId) == null)
                            if (answers[i] != "")
                                ac.InsertAnswer(testQuestionIds[i], student.StudentId, DateTime.Now, answers[i]);
                    }
                }
            }
            updateGrid();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog pictureDialog = new OpenFileDialog();

            if (pictureDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    var fileStream = pictureDialog.OpenFile();
                    Image image = new Bitmap(pictureDialog.FileName);
                    image.Save(ms, image.RawFormat);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    
                    string base64String = Convert.ToBase64String(imageBytes);
                    if (base64String.Length < 100000)
                    {
                        profile_picture_box.Image = Base64ToImage(base64String);
                        Task task = new Task(() => {
                            StudentController sc = new StudentController();
                            sc.UploadPicture(sc.GetStudent(Global.AUTHENTICATED_USER.UserId).StudentId, base64String);
                        });
                        task.Start();
                    }
                    else
                    {
                        MessageBox.Show("too big image. pick another");
                    }
                }
            }
        }
        public Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        private void announcement_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (announcement_list.SelectedItem != null)
            {
                string curItem = announcement_list.SelectedItem.ToString();

                AnnouncementController ac = new AnnouncementController();
                TeacherController tc = new TeacherController();

                var announcement = ac.GetAnnouncement(announcement_list.SelectedItem.ToString());
                var teacher = tc.GetTeacher(announcement.TeacherId);

                // On click shows a messageBox with the announcement + the Teacher who wrote it.

                if ((string)announcement_list.SelectedItem == curItem)
                {
                    MessageBox.Show((string)announcement_list.SelectedItem + " - Wrote by: " + teacher.Fname + " " + teacher.Lname);
                }
            }
            else
            {
                MessageBox.Show("Please select a item to show more details!");
            }
        }

        private void RefreshAnnounceList()
        {
            AnnouncementController ac = new AnnouncementController();

            StudentController sc = new StudentController();
            Student student = sc.GetStudent(Global.AUTHENTICATED_USER.UserId);

            announcement_list.Items.Clear();
            announcement_list.Items.AddRange(ac.GetAllAnnouncementsForClass(student.SchoolClassId).ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ChangePasswordForm pwForm = new ChangePasswordForm();
            pwForm.ShowDialog();
        }
    }
}