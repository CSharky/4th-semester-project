﻿namespace TeacherStudentApp.GUI
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.profile_tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.raank_txt_lbl = new System.Windows.Forms.Label();
            this.point_txt_lbl = new System.Windows.Forms.Label();
            this.lname_txt_lbl = new System.Windows.Forms.Label();
            this.fname_txt_lbl = new System.Windows.Forms.Label();
            this.last_name_lbl = new System.Windows.Forms.Label();
            this.points_lbl = new System.Windows.Forms.Label();
            this.rank_lbl = new System.Windows.Forms.Label();
            this.first_name_lbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.profile_picture_box = new System.Windows.Forms.PictureBox();
            this.exercises_tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.send_answer_btn = new System.Windows.Forms.Button();
            this.deadline_label = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.test_lbl = new System.Windows.Forms.Label();
            this.test_combo_box = new System.Windows.Forms.ComboBox();
            this.subject_lbl = new System.Windows.Forms.Label();
            this.subject_combo_box = new System.Windows.Forms.ComboBox();
            this.class_lbl = new System.Windows.Forms.Label();
            this.class_combo_box = new System.Windows.Forms.ComboBox();
            this.search_exercises_lbl = new System.Windows.Forms.Label();
            this.questions_grid = new System.Windows.Forms.DataGridView();
            this.TestQuestionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.questionText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AnswerText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.exercises_lbl = new System.Windows.Forms.Label();
            this.home_tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.announcement_list = new System.Windows.Forms.ListBox();
            this.class_list = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.teacher_announce_lbl = new System.Windows.Forms.Label();
            this.class_news_lbl = new System.Windows.Forms.Label();
            this.student_tab = new System.Windows.Forms.TabControl();
            this.rank_tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.rank_grid = new System.Windows.Forms.DataGridView();
            this.Rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClassName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.top10_lbl = new System.Windows.Forms.Label();
            this.changePW_btn = new System.Windows.Forms.Button();
            this.profile_tab.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profile_picture_box)).BeginInit();
            this.exercises_tab.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questions_grid)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.home_tab.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.student_tab.SuspendLayout();
            this.rank_tab.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rank_grid)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // profile_tab
            // 
            this.profile_tab.Controls.Add(this.tableLayoutPanel7);
            this.profile_tab.Location = new System.Drawing.Point(4, 22);
            this.profile_tab.Name = "profile_tab";
            this.profile_tab.Padding = new System.Windows.Forms.Padding(3);
            this.profile_tab.Size = new System.Drawing.Size(981, 569);
            this.profile_tab.TabIndex = 5;
            this.profile_tab.Text = "Profile";
            this.profile_tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.profile_picture_box, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.changePW_btn, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 563F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(975, 563);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel6.Controls.Add(this.raank_txt_lbl, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.point_txt_lbl, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.lname_txt_lbl, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.fname_txt_lbl, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.last_name_lbl, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.points_lbl, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.rank_lbl, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.first_name_lbl, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button1, 1, 4);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(159, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 5;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(813, 138);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // raank_txt_lbl
            // 
            this.raank_txt_lbl.AutoSize = true;
            this.raank_txt_lbl.Location = new System.Drawing.Point(84, 78);
            this.raank_txt_lbl.Name = "raank_txt_lbl";
            this.raank_txt_lbl.Size = new System.Drawing.Size(28, 13);
            this.raank_txt_lbl.TabIndex = 7;
            this.raank_txt_lbl.Text = "rank";
            // 
            // point_txt_lbl
            // 
            this.point_txt_lbl.AutoSize = true;
            this.point_txt_lbl.Location = new System.Drawing.Point(84, 52);
            this.point_txt_lbl.Name = "point_txt_lbl";
            this.point_txt_lbl.Size = new System.Drawing.Size(30, 13);
            this.point_txt_lbl.TabIndex = 6;
            this.point_txt_lbl.Text = "point";
            // 
            // lname_txt_lbl
            // 
            this.lname_txt_lbl.AutoSize = true;
            this.lname_txt_lbl.Location = new System.Drawing.Point(84, 26);
            this.lname_txt_lbl.Name = "lname_txt_lbl";
            this.lname_txt_lbl.Size = new System.Drawing.Size(35, 13);
            this.lname_txt_lbl.TabIndex = 5;
            this.lname_txt_lbl.Text = "lname";
            // 
            // fname_txt_lbl
            // 
            this.fname_txt_lbl.AutoSize = true;
            this.fname_txt_lbl.Location = new System.Drawing.Point(84, 0);
            this.fname_txt_lbl.Name = "fname_txt_lbl";
            this.fname_txt_lbl.Size = new System.Drawing.Size(36, 13);
            this.fname_txt_lbl.TabIndex = 4;
            this.fname_txt_lbl.Text = "fname";
            // 
            // last_name_lbl
            // 
            this.last_name_lbl.AutoSize = true;
            this.last_name_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.last_name_lbl.Location = new System.Drawing.Point(3, 26);
            this.last_name_lbl.Name = "last_name_lbl";
            this.last_name_lbl.Size = new System.Drawing.Size(49, 26);
            this.last_name_lbl.TabIndex = 1;
            this.last_name_lbl.Text = "Last Name:";
            // 
            // points_lbl
            // 
            this.points_lbl.AutoSize = true;
            this.points_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.points_lbl.Location = new System.Drawing.Point(3, 52);
            this.points_lbl.Name = "points_lbl";
            this.points_lbl.Size = new System.Drawing.Size(51, 15);
            this.points_lbl.TabIndex = 2;
            this.points_lbl.Text = "Points:";
            // 
            // rank_lbl
            // 
            this.rank_lbl.AutoSize = true;
            this.rank_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.rank_lbl.Location = new System.Drawing.Point(3, 78);
            this.rank_lbl.Name = "rank_lbl";
            this.rank_lbl.Size = new System.Drawing.Size(44, 15);
            this.rank_lbl.TabIndex = 3;
            this.rank_lbl.Text = "Rank:";
            // 
            // first_name_lbl
            // 
            this.first_name_lbl.AutoSize = true;
            this.first_name_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.first_name_lbl.Location = new System.Drawing.Point(3, 0);
            this.first_name_lbl.Name = "first_name_lbl";
            this.first_name_lbl.Size = new System.Drawing.Size(49, 26);
            this.first_name_lbl.TabIndex = 0;
            this.first_name_lbl.Text = "First Name:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(84, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Select Photo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // profile_picture_box
            // 
            this.profile_picture_box.Image = global::TeacherStudentApp.Properties.Resources.profilepic;
            this.profile_picture_box.Location = new System.Drawing.Point(3, 3);
            this.profile_picture_box.Name = "profile_picture_box";
            this.profile_picture_box.Size = new System.Drawing.Size(150, 150);
            this.profile_picture_box.TabIndex = 0;
            this.profile_picture_box.TabStop = false;
            // 
            // exercises_tab
            // 
            this.exercises_tab.Controls.Add(this.tableLayoutPanel2);
            this.exercises_tab.Controls.Add(this.tableLayoutPanel4);
            this.exercises_tab.Location = new System.Drawing.Point(4, 22);
            this.exercises_tab.Name = "exercises_tab";
            this.exercises_tab.Padding = new System.Windows.Forms.Padding(3);
            this.exercises_tab.Size = new System.Drawing.Size(981, 569);
            this.exercises_tab.TabIndex = 4;
            this.exercises_tab.Text = "Exercises";
            this.exercises_tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.questions_grid, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 538F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(975, 538);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.send_answer_btn);
            this.panel1.Controls.Add(this.deadline_label);
            this.panel1.Controls.Add(this.tableLayoutPanel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(490, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(482, 532);
            this.panel1.TabIndex = 1;
            // 
            // send_answer_btn
            // 
            this.send_answer_btn.Location = new System.Drawing.Point(99, 140);
            this.send_answer_btn.Name = "send_answer_btn";
            this.send_answer_btn.Size = new System.Drawing.Size(115, 23);
            this.send_answer_btn.TabIndex = 5;
            this.send_answer_btn.Text = "Send Answers";
            this.send_answer_btn.UseVisualStyleBackColor = true;
            this.send_answer_btn.Click += new System.EventHandler(this.send_answer_btn_Click);
            // 
            // deadline_label
            // 
            this.deadline_label.AutoSize = true;
            this.deadline_label.Location = new System.Drawing.Point(99, 107);
            this.deadline_label.Name = "deadline_label";
            this.deadline_label.Size = new System.Drawing.Size(84, 13);
            this.deadline_label.TabIndex = 4;
            this.deadline_label.Text = "Time remaining: ";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel5.Controls.Add(this.test_lbl, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.test_combo_box, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.subject_lbl, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.subject_combo_box, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.class_lbl, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.class_combo_box, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.search_exercises_lbl, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(482, 100);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // test_lbl
            // 
            this.test_lbl.AutoSize = true;
            this.test_lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.test_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.test_lbl.Location = new System.Drawing.Point(3, 75);
            this.test_lbl.Name = "test_lbl";
            this.test_lbl.Size = new System.Drawing.Size(90, 25);
            this.test_lbl.TabIndex = 3;
            this.test_lbl.Text = "Test";
            // 
            // test_combo_box
            // 
            this.test_combo_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.test_combo_box.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.test_combo_box.FormattingEnabled = true;
            this.test_combo_box.Location = new System.Drawing.Point(99, 78);
            this.test_combo_box.Name = "test_combo_box";
            this.test_combo_box.Size = new System.Drawing.Size(380, 21);
            this.test_combo_box.TabIndex = 2;
            this.test_combo_box.SelectedValueChanged += new System.EventHandler(this.test_combo_box_SelectedValueChanged);
            // 
            // subject_lbl
            // 
            this.subject_lbl.AutoSize = true;
            this.subject_lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subject_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.subject_lbl.Location = new System.Drawing.Point(3, 50);
            this.subject_lbl.Name = "subject_lbl";
            this.subject_lbl.Size = new System.Drawing.Size(90, 25);
            this.subject_lbl.TabIndex = 2;
            this.subject_lbl.Text = "Subject";
            // 
            // subject_combo_box
            // 
            this.subject_combo_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subject_combo_box.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subject_combo_box.FormattingEnabled = true;
            this.subject_combo_box.Location = new System.Drawing.Point(99, 53);
            this.subject_combo_box.Name = "subject_combo_box";
            this.subject_combo_box.Size = new System.Drawing.Size(380, 21);
            this.subject_combo_box.TabIndex = 1;
            this.subject_combo_box.SelectedValueChanged += new System.EventHandler(this.subject_combo_box_SelectedValueChanged);
            // 
            // class_lbl
            // 
            this.class_lbl.AutoSize = true;
            this.class_lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.class_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.class_lbl.Location = new System.Drawing.Point(3, 25);
            this.class_lbl.Name = "class_lbl";
            this.class_lbl.Size = new System.Drawing.Size(90, 25);
            this.class_lbl.TabIndex = 1;
            this.class_lbl.Text = "Class";
            // 
            // class_combo_box
            // 
            this.class_combo_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.class_combo_box.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.class_combo_box.Enabled = false;
            this.class_combo_box.FormattingEnabled = true;
            this.class_combo_box.Location = new System.Drawing.Point(99, 28);
            this.class_combo_box.Name = "class_combo_box";
            this.class_combo_box.Size = new System.Drawing.Size(380, 21);
            this.class_combo_box.TabIndex = 0;
            this.class_combo_box.SelectedValueChanged += new System.EventHandler(this.class_combo_box_SelectedValueChanged);
            // 
            // search_exercises_lbl
            // 
            this.search_exercises_lbl.AutoSize = true;
            this.search_exercises_lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.search_exercises_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.search_exercises_lbl.Location = new System.Drawing.Point(3, 0);
            this.search_exercises_lbl.Name = "search_exercises_lbl";
            this.search_exercises_lbl.Size = new System.Drawing.Size(90, 25);
            this.search_exercises_lbl.TabIndex = 0;
            this.search_exercises_lbl.Text = "Search";
            // 
            // questions_grid
            // 
            this.questions_grid.AllowUserToAddRows = false;
            this.questions_grid.AllowUserToDeleteRows = false;
            this.questions_grid.AllowUserToResizeColumns = false;
            this.questions_grid.AllowUserToResizeRows = false;
            this.questions_grid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.questions_grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.questions_grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.questions_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.questions_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TestQuestionId,
            this.questionText,
            this.AnswerText});
            this.questions_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.questions_grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.questions_grid.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.questions_grid.Location = new System.Drawing.Point(3, 3);
            this.questions_grid.MultiSelect = false;
            this.questions_grid.Name = "questions_grid";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.questions_grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.questions_grid.RowHeadersVisible = false;
            this.questions_grid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.questions_grid.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.questions_grid.RowTemplate.Height = 30;
            this.questions_grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.questions_grid.Size = new System.Drawing.Size(481, 532);
            this.questions_grid.TabIndex = 4;
            // 
            // TestQuestionId
            // 
            this.TestQuestionId.DataPropertyName = "TestQuestionId";
            this.TestQuestionId.HeaderText = "ID";
            this.TestQuestionId.Name = "TestQuestionId";
            this.TestQuestionId.ReadOnly = true;
            this.TestQuestionId.Width = 40;
            // 
            // questionText
            // 
            this.questionText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.questionText.DataPropertyName = "QuestionText";
            this.questionText.FillWeight = 99.4012F;
            this.questionText.HeaderText = "Question";
            this.questionText.Name = "questionText";
            this.questionText.ReadOnly = true;
            // 
            // AnswerText
            // 
            this.AnswerText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AnswerText.DataPropertyName = "AnswerText";
            this.AnswerText.FillWeight = 100.5988F;
            this.AnswerText.HeaderText = "Answer";
            this.AnswerText.Name = "AnswerText";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.exercises_lbl, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(975, 25);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // exercises_lbl
            // 
            this.exercises_lbl.AutoSize = true;
            this.exercises_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.exercises_lbl.Location = new System.Drawing.Point(3, 0);
            this.exercises_lbl.Name = "exercises_lbl";
            this.exercises_lbl.Size = new System.Drawing.Size(77, 20);
            this.exercises_lbl.TabIndex = 2;
            this.exercises_lbl.Text = "Exercises";
            // 
            // home_tab
            // 
            this.home_tab.Controls.Add(this.tableLayoutPanel1);
            this.home_tab.Controls.Add(this.tableLayoutPanel3);
            this.home_tab.Location = new System.Drawing.Point(4, 22);
            this.home_tab.Name = "home_tab";
            this.home_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_tab.Size = new System.Drawing.Size(981, 569);
            this.home_tab.TabIndex = 0;
            this.home_tab.Text = "Home";
            this.home_tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.announcement_list, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.class_list, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 538F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(975, 538);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // announcement_list
            // 
            this.announcement_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.announcement_list.FormattingEnabled = true;
            this.announcement_list.Location = new System.Drawing.Point(3, 3);
            this.announcement_list.Name = "announcement_list";
            this.announcement_list.Size = new System.Drawing.Size(481, 532);
            this.announcement_list.TabIndex = 0;
            this.announcement_list.SelectedIndexChanged += new System.EventHandler(this.announcement_list_SelectedIndexChanged);
            // 
            // class_list
            // 
            this.class_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.class_list.FormattingEnabled = true;
            this.class_list.Location = new System.Drawing.Point(490, 3);
            this.class_list.Name = "class_list";
            this.class_list.Size = new System.Drawing.Size(482, 532);
            this.class_list.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.teacher_announce_lbl, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.class_news_lbl, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(975, 25);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // teacher_announce_lbl
            // 
            this.teacher_announce_lbl.AutoSize = true;
            this.teacher_announce_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.teacher_announce_lbl.Location = new System.Drawing.Point(3, 0);
            this.teacher_announce_lbl.Name = "teacher_announce_lbl";
            this.teacher_announce_lbl.Size = new System.Drawing.Size(196, 20);
            this.teacher_announce_lbl.TabIndex = 2;
            this.teacher_announce_lbl.Text = "Teachers Announcements";
            // 
            // class_news_lbl
            // 
            this.class_news_lbl.AutoSize = true;
            this.class_news_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.class_news_lbl.Location = new System.Drawing.Point(490, 0);
            this.class_news_lbl.Name = "class_news_lbl";
            this.class_news_lbl.Size = new System.Drawing.Size(91, 20);
            this.class_news_lbl.TabIndex = 1;
            this.class_news_lbl.Text = "Class News";
            // 
            // student_tab
            // 
            this.student_tab.Controls.Add(this.home_tab);
            this.student_tab.Controls.Add(this.exercises_tab);
            this.student_tab.Controls.Add(this.profile_tab);
            this.student_tab.Controls.Add(this.rank_tab);
            this.student_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.student_tab.Location = new System.Drawing.Point(0, 0);
            this.student_tab.Name = "student_tab";
            this.student_tab.SelectedIndex = 0;
            this.student_tab.Size = new System.Drawing.Size(989, 595);
            this.student_tab.TabIndex = 0;
            // 
            // rank_tab
            // 
            this.rank_tab.Controls.Add(this.tableLayoutPanel8);
            this.rank_tab.Controls.Add(this.tableLayoutPanel10);
            this.rank_tab.Location = new System.Drawing.Point(4, 22);
            this.rank_tab.Name = "rank_tab";
            this.rank_tab.Padding = new System.Windows.Forms.Padding(3);
            this.rank_tab.Size = new System.Drawing.Size(981, 569);
            this.rank_tab.TabIndex = 6;
            this.rank_tab.Text = "Rank";
            this.rank_tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.rank_grid, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 538F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(975, 538);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // rank_grid
            // 
            this.rank_grid.AllowUserToAddRows = false;
            this.rank_grid.AllowUserToDeleteRows = false;
            this.rank_grid.AllowUserToResizeColumns = false;
            this.rank_grid.AllowUserToResizeRows = false;
            this.rank_grid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.rank_grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.rank_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rank_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Rank,
            this.FirstName,
            this.LastName,
            this.ClassName,
            this.Score});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.rank_grid.DefaultCellStyle = dataGridViewCellStyle7;
            this.rank_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rank_grid.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rank_grid.Location = new System.Drawing.Point(3, 3);
            this.rank_grid.MultiSelect = false;
            this.rank_grid.Name = "rank_grid";
            this.rank_grid.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.rank_grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.rank_grid.RowHeadersVisible = false;
            this.rank_grid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.rank_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.rank_grid.Size = new System.Drawing.Size(481, 532);
            this.rank_grid.TabIndex = 0;
            // 
            // Rank
            // 
            this.Rank.DataPropertyName = "Rank";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rank.DefaultCellStyle = dataGridViewCellStyle5;
            this.Rank.HeaderText = "Rank";
            this.Rank.Name = "Rank";
            this.Rank.ReadOnly = true;
            this.Rank.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Rank.Width = 45;
            // 
            // FirstName
            // 
            this.FirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            this.FirstName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // LastName
            // 
            this.LastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            this.LastName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ClassName
            // 
            this.ClassName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ClassName.DataPropertyName = "ClassName";
            this.ClassName.HeaderText = "Class";
            this.ClassName.Name = "ClassName";
            this.ClassName.ReadOnly = true;
            this.ClassName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Score
            // 
            this.Score.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Score.DataPropertyName = "Score";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Score.DefaultCellStyle = dataGridViewCellStyle6;
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            this.Score.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel10.Controls.Add(this.top10_lbl, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(975, 25);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // top10_lbl
            // 
            this.top10_lbl.AutoSize = true;
            this.top10_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.top10_lbl.Location = new System.Drawing.Point(3, 0);
            this.top10_lbl.Name = "top10_lbl";
            this.top10_lbl.Size = new System.Drawing.Size(58, 20);
            this.top10_lbl.TabIndex = 2;
            this.top10_lbl.Text = "Top 10";
            // 
            // changePW_btn
            // 
            this.changePW_btn.Location = new System.Drawing.Point(3, 159);
            this.changePW_btn.Name = "changePW_btn";
            this.changePW_btn.Size = new System.Drawing.Size(150, 22);
            this.changePW_btn.TabIndex = 9;
            this.changePW_btn.Text = "Change password";
            this.changePW_btn.UseVisualStyleBackColor = true;
            this.changePW_btn.Click += new System.EventHandler(this.button2_Click);
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 595);
            this.Controls.Add(this.student_tab);
            this.Name = "StudentForm";
            this.Text = "Student: ";
            this.Load += new System.EventHandler(this.StudentForm_Load);
            this.profile_tab.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profile_picture_box)).EndInit();
            this.exercises_tab.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questions_grid)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.home_tab.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.student_tab.ResumeLayout(false);
            this.rank_tab.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rank_grid)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage profile_tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label raank_txt_lbl;
        private System.Windows.Forms.Label point_txt_lbl;
        private System.Windows.Forms.Label lname_txt_lbl;
        private System.Windows.Forms.Label fname_txt_lbl;
        private System.Windows.Forms.Label last_name_lbl;
        private System.Windows.Forms.Label points_lbl;
        private System.Windows.Forms.Label rank_lbl;
        private System.Windows.Forms.Label first_name_lbl;
        private System.Windows.Forms.PictureBox profile_picture_box;
        private System.Windows.Forms.TabPage exercises_tab;
								private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label test_lbl;
        private System.Windows.Forms.ComboBox test_combo_box;
        private System.Windows.Forms.Label subject_lbl;
        private System.Windows.Forms.ComboBox subject_combo_box;
        private System.Windows.Forms.Label class_lbl;
        private System.Windows.Forms.ComboBox class_combo_box;
        private System.Windows.Forms.Label search_exercises_lbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label exercises_lbl;
        private System.Windows.Forms.TabPage home_tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox announcement_list;
        private System.Windows.Forms.ListBox class_list;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label teacher_announce_lbl;
        private System.Windows.Forms.Label class_news_lbl;
        private System.Windows.Forms.TabControl student_tab;
								private System.Windows.Forms.TabPage rank_tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
								private System.Windows.Forms.Label top10_lbl;
        private System.Windows.Forms.Label deadline_label;
								private System.Windows.Forms.DataGridView questions_grid;
								private System.Windows.Forms.Button send_answer_btn;
								private System.Windows.Forms.DataGridViewTextBoxColumn TestQuestionId;
								private System.Windows.Forms.DataGridViewTextBoxColumn questionText;
                                private System.Windows.Forms.DataGridViewTextBoxColumn AnswerText;
                                private System.Windows.Forms.Button button1;
																																private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
																																private System.Windows.Forms.DataGridView rank_grid;
																																private System.Windows.Forms.DataGridViewTextBoxColumn Rank;
																																private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
																																private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
																																private System.Windows.Forms.DataGridViewTextBoxColumn ClassName;
																																private System.Windows.Forms.DataGridViewTextBoxColumn Score;
                                                                                                                                private System.Windows.Forms.Button changePW_btn;



    }
}