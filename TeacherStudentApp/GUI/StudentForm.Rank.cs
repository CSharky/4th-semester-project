﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.GUI
{
		/// <summary>
		/// [Rank]
		/// Here are placed all the methods for the 
		/// form controls of the Rank tab in the
		/// StudentForm
		/// </summary>
		public partial class StudentForm
		{
				private void updateRankGrid()
				{
						rank_grid.DataSource = null;
						
						StudentController sc = new StudentController();
						rank_grid.AutoGenerateColumns = true;
						IList<StudentWithClassName> list = sc.GetTop10Students();
						
						rank_grid.DataSource = list;

						//Cell autoresize and wrap mode settings.
						rank_grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

						rank_grid.Columns["Rank"].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
						rank_grid.Columns["Rank"].Width = 45;
						rank_grid.Columns["FirstName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
						rank_grid.Columns["LastName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
						rank_grid.Columns["ClassName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
						rank_grid.Columns["Score"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

						//Header values.
						rank_grid.Columns["Rank"].HeaderText = "Rank";
						rank_grid.Columns["FirstName"].HeaderText = "First Name";
						rank_grid.Columns["LastName"].HeaderText = "Last Name";						
						rank_grid.Columns["ClassName"].HeaderText = "Class";
						rank_grid.Columns["Score"].HeaderText = "Score";

						int i = 0;
						foreach (DataGridViewRow r in rank_grid.Rows)
						{
								r.Cells["Rank"].Value = ++i;
						}
				}
				// TODO Delete Dummy Users
				// TODO Make the rankGrid to update when Rank tab is clicked
		}
}
