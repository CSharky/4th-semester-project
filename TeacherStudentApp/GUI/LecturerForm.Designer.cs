﻿namespace TeacherStudentApp.GUI
{
    partial class LecturerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.announce_Tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.announcement_list_lbl = new System.Windows.Forms.Label();
            this.announcement_list = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.announce_text = new System.Windows.Forms.TextBox();
            this.announce_create_btn = new System.Windows.Forms.Button();
            this.announcement_lbl = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.announce_msg_lbl = new System.Windows.Forms.Label();
            this.announce_edit_msg_text = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.announce_update_btn = new System.Windows.Forms.Button();
            this.announce_remove_btn = new System.Windows.Forms.Button();
            this.announce_edit_btn = new System.Windows.Forms.Button();
            this.announce_edit_cancel_btn = new System.Windows.Forms.Button();
            this.verifyAnswers_tab = new System.Windows.Forms.TabPage();
            this.verifyTests_layout1 = new System.Windows.Forms.TableLayoutPanel();
            this.verification_grid = new System.Windows.Forms.DataGridView();
            this.AnswerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AnswerText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsCorrect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Points = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verifyTests_layout2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.verify_btn = new System.Windows.Forms.Button();
            this.question_lbl2 = new System.Windows.Forms.Label();
            this.test_lbl2 = new System.Windows.Forms.Label();
            this.subject_lbl2 = new System.Windows.Forms.Label();
            this.class_lbl2 = new System.Windows.Forms.Label();
            this.class_comboBox2 = new System.Windows.Forms.ComboBox();
            this.subject_comboBox2 = new System.Windows.Forms.ComboBox();
            this.test_comboBox2 = new System.Windows.Forms.ComboBox();
            this.question_comboBox2 = new System.Windows.Forms.ComboBox();
            this.answers_lbl = new System.Windows.Forms.Label();
            this.createTests_tab = new System.Windows.Forms.TabPage();
            this.createTests_layout1 = new System.Windows.Forms.TableLayoutPanel();
            this.tests_comboBox = new System.Windows.Forms.ComboBox();
            this.createTests_layout4 = new System.Windows.Forms.TableLayoutPanel();
            this.deleteTest_btn = new System.Windows.Forms.Button();
            this.deleteSelected_btn = new System.Windows.Forms.Button();
            this.createTests_layout2 = new System.Windows.Forms.TableLayoutPanel();
            this.sendTest_lbl = new System.Windows.Forms.Label();
            this.addToTest_btn = new System.Windows.Forms.Button();
            this.deleteQuestion_btn = new System.Windows.Forms.Button();
            this.sendToClass_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.class_comboBox = new System.Windows.Forms.ComboBox();
            this.subject_comboBox = new System.Windows.Forms.ComboBox();
            this.question_lbl = new System.Windows.Forms.Label();
            this.createTest_layout5 = new System.Windows.Forms.TableLayoutPanel();
            this.question_tBox = new System.Windows.Forms.TextBox();
            this.createQuestion_btn = new System.Windows.Forms.Button();
            this.questions_tbl = new System.Windows.Forms.DataGridView();
            this.testDeadline_dtPicker = new System.Windows.Forms.DateTimePicker();
            this.createTest_btn = new System.Windows.Forms.Button();
            this.questions_comboBox = new System.Windows.Forms.ComboBox();
            this.testname_tBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lecturer_tabCtr = new System.Windows.Forms.TabControl();
            this.announce_Tab.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.verifyAnswers_tab.SuspendLayout();
            this.verifyTests_layout1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verification_grid)).BeginInit();
            this.verifyTests_layout2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.createTests_tab.SuspendLayout();
            this.createTests_layout1.SuspendLayout();
            this.createTests_layout4.SuspendLayout();
            this.createTests_layout2.SuspendLayout();
            this.createTest_layout5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questions_tbl)).BeginInit();
            this.lecturer_tabCtr.SuspendLayout();
            this.SuspendLayout();
            // 
            // announce_Tab
            // 
            this.announce_Tab.Controls.Add(this.tableLayoutPanel2);
            this.announce_Tab.Location = new System.Drawing.Point(4, 22);
            this.announce_Tab.Name = "announce_Tab";
            this.announce_Tab.Padding = new System.Windows.Forms.Padding(3);
            this.announce_Tab.Size = new System.Drawing.Size(981, 569);
            this.announce_Tab.TabIndex = 2;
            this.announce_Tab.Text = "Create Announcement";
            this.announce_Tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 563F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(975, 563);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.announcement_list_lbl, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.announcement_list, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.announcement_lbl, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(481, 557);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // announcement_list_lbl
            // 
            this.announcement_list_lbl.AutoSize = true;
            this.announcement_list_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.announcement_list_lbl.Location = new System.Drawing.Point(3, 60);
            this.announcement_list_lbl.Name = "announcement_list_lbl";
            this.announcement_list_lbl.Size = new System.Drawing.Size(151, 20);
            this.announcement_list_lbl.TabIndex = 4;
            this.announcement_list_lbl.Text = "Announcement List:";
            // 
            // announcement_list
            // 
            this.announcement_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.announcement_list.FormattingEnabled = true;
            this.announcement_list.Location = new System.Drawing.Point(3, 90);
            this.announcement_list.Name = "announcement_list";
            this.announcement_list.Size = new System.Drawing.Size(475, 464);
            this.announcement_list.TabIndex = 3;
            this.announcement_list.SelectedIndexChanged += new System.EventHandler(this.announcement_list_SelectedIndexChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.announce_text, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.announce_create_btn, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 30);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(475, 26);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // announce_text
            // 
            this.announce_text.Dock = System.Windows.Forms.DockStyle.Fill;
            this.announce_text.Location = new System.Drawing.Point(3, 3);
            this.announce_text.Name = "announce_text";
            this.announce_text.Size = new System.Drawing.Size(350, 20);
            this.announce_text.TabIndex = 0;
            // 
            // announce_create_btn
            // 
            this.announce_create_btn.AutoSize = true;
            this.announce_create_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.announce_create_btn.Location = new System.Drawing.Point(359, 3);
            this.announce_create_btn.Name = "announce_create_btn";
            this.announce_create_btn.Size = new System.Drawing.Size(113, 20);
            this.announce_create_btn.TabIndex = 1;
            this.announce_create_btn.Text = "Create Announcement";
            this.announce_create_btn.UseVisualStyleBackColor = true;
            this.announce_create_btn.Click += new System.EventHandler(this.announce_create_btn_Click);
            // 
            // announcement_lbl
            // 
            this.announcement_lbl.AutoSize = true;
            this.announcement_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.announcement_lbl.Location = new System.Drawing.Point(3, 0);
            this.announcement_lbl.Name = "announcement_lbl";
            this.announcement_lbl.Size = new System.Drawing.Size(118, 20);
            this.announcement_lbl.TabIndex = 2;
            this.announcement_lbl.Text = "Announcement";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(490, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(482, 557);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(476, 272);
            this.tableLayoutPanel7.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel6.Controls.Add(this.announce_msg_lbl, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.announce_edit_msg_text, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(470, 26);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // announce_msg_lbl
            // 
            this.announce_msg_lbl.AutoSize = true;
            this.announce_msg_lbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_msg_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.announce_msg_lbl.Location = new System.Drawing.Point(3, 0);
            this.announce_msg_lbl.Name = "announce_msg_lbl";
            this.announce_msg_lbl.Size = new System.Drawing.Size(64, 15);
            this.announce_msg_lbl.TabIndex = 0;
            this.announce_msg_lbl.Text = "Message:";
            // 
            // announce_edit_msg_text
            // 
            this.announce_edit_msg_text.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_edit_msg_text.Enabled = false;
            this.announce_edit_msg_text.Location = new System.Drawing.Point(73, 3);
            this.announce_edit_msg_text.Name = "announce_edit_msg_text";
            this.announce_edit_msg_text.Size = new System.Drawing.Size(394, 20);
            this.announce_edit_msg_text.TabIndex = 1;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.Controls.Add(this.announce_update_btn, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.announce_remove_btn, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.announce_edit_btn, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.announce_edit_cancel_btn, 3, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 35);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(470, 100);
            this.tableLayoutPanel8.TabIndex = 4;
            // 
            // announce_update_btn
            // 
            this.announce_update_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_update_btn.Enabled = false;
            this.announce_update_btn.Location = new System.Drawing.Point(237, 3);
            this.announce_update_btn.Name = "announce_update_btn";
            this.announce_update_btn.Size = new System.Drawing.Size(111, 23);
            this.announce_update_btn.TabIndex = 2;
            this.announce_update_btn.Text = "Update";
            this.announce_update_btn.UseVisualStyleBackColor = true;
            this.announce_update_btn.Click += new System.EventHandler(this.announce_update_btn_Click);
            // 
            // announce_remove_btn
            // 
            this.announce_remove_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_remove_btn.Location = new System.Drawing.Point(120, 3);
            this.announce_remove_btn.Name = "announce_remove_btn";
            this.announce_remove_btn.Size = new System.Drawing.Size(111, 23);
            this.announce_remove_btn.TabIndex = 1;
            this.announce_remove_btn.Text = "Remove";
            this.announce_remove_btn.UseVisualStyleBackColor = true;
            this.announce_remove_btn.Click += new System.EventHandler(this.announce_remove_btn_Click);
            // 
            // announce_edit_btn
            // 
            this.announce_edit_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_edit_btn.Location = new System.Drawing.Point(3, 3);
            this.announce_edit_btn.Name = "announce_edit_btn";
            this.announce_edit_btn.Size = new System.Drawing.Size(111, 23);
            this.announce_edit_btn.TabIndex = 0;
            this.announce_edit_btn.Text = "Edit";
            this.announce_edit_btn.UseVisualStyleBackColor = true;
            this.announce_edit_btn.Click += new System.EventHandler(this.announce_edit_btn_Click);
            // 
            // announce_edit_cancel_btn
            // 
            this.announce_edit_cancel_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.announce_edit_cancel_btn.Location = new System.Drawing.Point(354, 3);
            this.announce_edit_cancel_btn.Name = "announce_edit_cancel_btn";
            this.announce_edit_cancel_btn.Size = new System.Drawing.Size(113, 23);
            this.announce_edit_cancel_btn.TabIndex = 3;
            this.announce_edit_cancel_btn.Text = "Cancel";
            this.announce_edit_cancel_btn.UseVisualStyleBackColor = true;
            this.announce_edit_cancel_btn.Click += new System.EventHandler(this.announce_edit_cancel_btn_Click);
            // 
            // verifyAnswers_tab
            // 
            this.verifyAnswers_tab.Controls.Add(this.verifyTests_layout1);
            this.verifyAnswers_tab.Location = new System.Drawing.Point(4, 22);
            this.verifyAnswers_tab.Name = "verifyAnswers_tab";
            this.verifyAnswers_tab.Padding = new System.Windows.Forms.Padding(3);
            this.verifyAnswers_tab.Size = new System.Drawing.Size(981, 569);
            this.verifyAnswers_tab.TabIndex = 1;
            this.verifyAnswers_tab.Text = "Verify Answers";
            this.verifyAnswers_tab.UseVisualStyleBackColor = true;
            // 
            // verifyTests_layout1
            // 
            this.verifyTests_layout1.ColumnCount = 2;
            this.verifyTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.verifyTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.verifyTests_layout1.Controls.Add(this.verification_grid, 0, 1);
            this.verifyTests_layout1.Controls.Add(this.verifyTests_layout2, 1, 1);
            this.verifyTests_layout1.Controls.Add(this.answers_lbl, 0, 0);
            this.verifyTests_layout1.Dock = System.Windows.Forms.DockStyle.Top;
            this.verifyTests_layout1.Location = new System.Drawing.Point(3, 3);
            this.verifyTests_layout1.Name = "verifyTests_layout1";
            this.verifyTests_layout1.RowCount = 2;
            this.verifyTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.verifyTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 537F));
            this.verifyTests_layout1.Size = new System.Drawing.Size(975, 566);
            this.verifyTests_layout1.TabIndex = 0;
            // 
            // verification_grid
            // 
            this.verification_grid.AllowUserToAddRows = false;
            this.verification_grid.AllowUserToDeleteRows = false;
            this.verification_grid.AllowUserToResizeColumns = false;
            this.verification_grid.AllowUserToResizeRows = false;
            this.verification_grid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.verification_grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.verification_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.verification_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AnswerId,
            this.StudentId,
            this.FirstName,
            this.LastName,
            this.AnswerText,
            this.IsCorrect,
            this.Points});
            this.verification_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verification_grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.verification_grid.Location = new System.Drawing.Point(3, 32);
            this.verification_grid.MultiSelect = false;
            this.verification_grid.Name = "verification_grid";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.verification_grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.verification_grid.RowHeadersVisible = false;
            this.verification_grid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.verification_grid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.verification_grid.Size = new System.Drawing.Size(481, 531);
            this.verification_grid.TabIndex = 4;
            // 
            // AnswerId
            // 
            this.AnswerId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AnswerId.DataPropertyName = "AnswerId";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AnswerId.DefaultCellStyle = dataGridViewCellStyle2;
            this.AnswerId.HeaderText = "ID";
            this.AnswerId.Name = "AnswerId";
            this.AnswerId.ReadOnly = true;
            // 
            // StudentId
            // 
            this.StudentId.DataPropertyName = "StudentId";
            this.StudentId.HeaderText = "SId";
            this.StudentId.Name = "StudentId";
            this.StudentId.ReadOnly = true;
            this.StudentId.Visible = false;
            // 
            // FirstName
            // 
            this.FirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // AnswerText
            // 
            this.AnswerText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AnswerText.DataPropertyName = "AnswerText";
            this.AnswerText.HeaderText = "Answer";
            this.AnswerText.Name = "AnswerText";
            this.AnswerText.ReadOnly = true;
            // 
            // IsCorrect
            // 
            this.IsCorrect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IsCorrect.DataPropertyName = "IsCorrect";
            this.IsCorrect.FalseValue = "False";
            this.IsCorrect.HeaderText = "Correct?";
            this.IsCorrect.IndeterminateValue = "";
            this.IsCorrect.Name = "IsCorrect";
            this.IsCorrect.TrueValue = "True";
            // 
            // Points
            // 
            this.Points.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Points.DataPropertyName = "Points";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Points.DefaultCellStyle = dataGridViewCellStyle3;
            this.Points.HeaderText = "Points";
            this.Points.Name = "Points";
            // 
            // verifyTests_layout2
            // 
            this.verifyTests_layout2.ColumnCount = 1;
            this.verifyTests_layout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.verifyTests_layout2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.verifyTests_layout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verifyTests_layout2.Location = new System.Drawing.Point(490, 32);
            this.verifyTests_layout2.Name = "verifyTests_layout2";
            this.verifyTests_layout2.RowCount = 1;
            this.verifyTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.31507F));
            this.verifyTests_layout2.Size = new System.Drawing.Size(482, 531);
            this.verifyTests_layout2.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.12605F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.87395F));
            this.tableLayoutPanel1.Controls.Add(this.verify_btn, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.question_lbl2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.test_lbl2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.subject_lbl2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.class_lbl2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.class_comboBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.subject_comboBox2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.test_comboBox2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.question_comboBox2, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.87719F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.12281F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 418F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(476, 525);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // verify_btn
            // 
            this.verify_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.verify_btn.Location = new System.Drawing.Point(74, 109);
            this.verify_btn.Name = "verify_btn";
            this.verify_btn.Size = new System.Drawing.Size(169, 29);
            this.verify_btn.TabIndex = 0;
            this.verify_btn.Text = "Verify";
            this.verify_btn.UseVisualStyleBackColor = true;
            this.verify_btn.Click += new System.EventHandler(this.verify_btn_Click);
            // 
            // question_lbl2
            // 
            this.question_lbl2.AutoSize = true;
            this.question_lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.question_lbl2.Location = new System.Drawing.Point(3, 77);
            this.question_lbl2.Name = "question_lbl2";
            this.question_lbl2.Size = new System.Drawing.Size(64, 16);
            this.question_lbl2.TabIndex = 5;
            this.question_lbl2.Text = "Question:";
            // 
            // test_lbl2
            // 
            this.test_lbl2.AutoSize = true;
            this.test_lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.test_lbl2.Location = new System.Drawing.Point(3, 52);
            this.test_lbl2.Name = "test_lbl2";
            this.test_lbl2.Size = new System.Drawing.Size(38, 16);
            this.test_lbl2.TabIndex = 4;
            this.test_lbl2.Text = "Test:";
            // 
            // subject_lbl2
            // 
            this.subject_lbl2.AutoSize = true;
            this.subject_lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.subject_lbl2.Location = new System.Drawing.Point(3, 26);
            this.subject_lbl2.Name = "subject_lbl2";
            this.subject_lbl2.Size = new System.Drawing.Size(56, 16);
            this.subject_lbl2.TabIndex = 2;
            this.subject_lbl2.Text = "Subject:";
            // 
            // class_lbl2
            // 
            this.class_lbl2.AutoSize = true;
            this.class_lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.class_lbl2.Location = new System.Drawing.Point(3, 0);
            this.class_lbl2.Name = "class_lbl2";
            this.class_lbl2.Size = new System.Drawing.Size(45, 16);
            this.class_lbl2.TabIndex = 3;
            this.class_lbl2.Text = "Class:";
            // 
            // class_comboBox2
            // 
            this.class_comboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.class_comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.class_comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.class_comboBox2.FormattingEnabled = true;
            this.class_comboBox2.Location = new System.Drawing.Point(74, 3);
            this.class_comboBox2.Name = "class_comboBox2";
            this.class_comboBox2.Size = new System.Drawing.Size(399, 24);
            this.class_comboBox2.TabIndex = 2;
            this.class_comboBox2.SelectedValueChanged += new System.EventHandler(this.class_comboBox2_SelectedValueChanged);
            // 
            // subject_comboBox2
            // 
            this.subject_comboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subject_comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subject_comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.subject_comboBox2.FormattingEnabled = true;
            this.subject_comboBox2.Location = new System.Drawing.Point(74, 29);
            this.subject_comboBox2.Name = "subject_comboBox2";
            this.subject_comboBox2.Size = new System.Drawing.Size(399, 24);
            this.subject_comboBox2.TabIndex = 1;
            this.subject_comboBox2.SelectedValueChanged += new System.EventHandler(this.subject_comboBox2_SelectedValueChanged);
            // 
            // test_comboBox2
            // 
            this.test_comboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.test_comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.test_comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.test_comboBox2.FormattingEnabled = true;
            this.test_comboBox2.Location = new System.Drawing.Point(74, 55);
            this.test_comboBox2.Name = "test_comboBox2";
            this.test_comboBox2.Size = new System.Drawing.Size(399, 24);
            this.test_comboBox2.TabIndex = 3;
            this.test_comboBox2.SelectedValueChanged += new System.EventHandler(this.test_comboBox2_SelectedValueChanged);
            // 
            // question_comboBox2
            // 
            this.question_comboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.question_comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.question_comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.question_comboBox2.FormattingEnabled = true;
            this.question_comboBox2.Location = new System.Drawing.Point(74, 80);
            this.question_comboBox2.Name = "question_comboBox2";
            this.question_comboBox2.Size = new System.Drawing.Size(399, 24);
            this.question_comboBox2.TabIndex = 4;
            this.question_comboBox2.SelectedValueChanged += new System.EventHandler(this.question_comboBox2_SelectedValueChanged);
            // 
            // answers_lbl
            // 
            this.answers_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.answers_lbl.AutoSize = true;
            this.answers_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.answers_lbl.Location = new System.Drawing.Point(3, 4);
            this.answers_lbl.Name = "answers_lbl";
            this.answers_lbl.Size = new System.Drawing.Size(74, 20);
            this.answers_lbl.TabIndex = 2;
            this.answers_lbl.Text = "Answers:";
            // 
            // createTests_tab
            // 
            this.createTests_tab.Controls.Add(this.createTests_layout1);
            this.createTests_tab.Location = new System.Drawing.Point(4, 22);
            this.createTests_tab.Name = "createTests_tab";
            this.createTests_tab.Padding = new System.Windows.Forms.Padding(3);
            this.createTests_tab.Size = new System.Drawing.Size(981, 569);
            this.createTests_tab.TabIndex = 0;
            this.createTests_tab.Text = "Create Tests";
            this.createTests_tab.UseVisualStyleBackColor = true;
            // 
            // createTests_layout1
            // 
            this.createTests_layout1.ColumnCount = 4;
            this.createTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.createTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.createTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.createTests_layout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.createTests_layout1.Controls.Add(this.tests_comboBox, 1, 2);
            this.createTests_layout1.Controls.Add(this.createTests_layout4, 1, 4);
            this.createTests_layout1.Controls.Add(this.createTests_layout2, 0, 3);
            this.createTests_layout1.Controls.Add(this.question_lbl, 0, 0);
            this.createTests_layout1.Controls.Add(this.createTest_layout5, 0, 1);
            this.createTests_layout1.Controls.Add(this.questions_tbl, 1, 3);
            this.createTests_layout1.Controls.Add(this.testDeadline_dtPicker, 2, 0);
            this.createTests_layout1.Controls.Add(this.createTest_btn, 3, 1);
            this.createTests_layout1.Controls.Add(this.questions_comboBox, 0, 2);
            this.createTests_layout1.Controls.Add(this.testname_tBox, 1, 1);
            this.createTests_layout1.Controls.Add(this.label3, 1, 0);
            this.createTests_layout1.Dock = System.Windows.Forms.DockStyle.Top;
            this.createTests_layout1.Location = new System.Drawing.Point(3, 3);
            this.createTests_layout1.Name = "createTests_layout1";
            this.createTests_layout1.RowCount = 5;
            this.createTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout1.Size = new System.Drawing.Size(975, 569);
            this.createTests_layout1.TabIndex = 5;
            // 
            // tests_comboBox
            // 
            this.createTests_layout1.SetColumnSpan(this.tests_comboBox, 3);
            this.tests_comboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.tests_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tests_comboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tests_comboBox.FormattingEnabled = true;
            this.tests_comboBox.Location = new System.Drawing.Point(490, 67);
            this.tests_comboBox.Name = "tests_comboBox";
            this.tests_comboBox.Size = new System.Drawing.Size(482, 24);
            this.tests_comboBox.TabIndex = 3;
            this.tests_comboBox.SelectedValueChanged += new System.EventHandler(this.tests_comboBox_SelectedValueChanged);
            // 
            // createTests_layout4
            // 
            this.createTests_layout4.ColumnCount = 5;
            this.createTests_layout1.SetColumnSpan(this.createTests_layout4, 3);
            this.createTests_layout4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.921162F));
            this.createTests_layout4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.58921F));
            this.createTests_layout4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.60166F));
            this.createTests_layout4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.58921F));
            this.createTests_layout4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.091287F));
            this.createTests_layout4.Controls.Add(this.deleteTest_btn, 3, 0);
            this.createTests_layout4.Controls.Add(this.deleteSelected_btn, 1, 0);
            this.createTests_layout4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createTests_layout4.Location = new System.Drawing.Point(490, 483);
            this.createTests_layout4.Name = "createTests_layout4";
            this.createTests_layout4.RowCount = 1;
            this.createTests_layout4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.createTests_layout4.Size = new System.Drawing.Size(482, 83);
            this.createTests_layout4.TabIndex = 5;
            // 
            // deleteTest_btn
            // 
            this.deleteTest_btn.AutoSize = true;
            this.deleteTest_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteTest_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteTest_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteTest_btn.Location = new System.Drawing.Point(259, 3);
            this.deleteTest_btn.Name = "deleteTest_btn";
            this.deleteTest_btn.Size = new System.Drawing.Size(180, 26);
            this.deleteTest_btn.TabIndex = 6;
            this.deleteTest_btn.Text = "Delete Test";
            this.deleteTest_btn.UseVisualStyleBackColor = true;
            this.deleteTest_btn.Click += new System.EventHandler(this.deleteTest_btn_Click);
            // 
            // deleteSelected_btn
            // 
            this.deleteSelected_btn.AutoSize = true;
            this.deleteSelected_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteSelected_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteSelected_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelected_btn.Location = new System.Drawing.Point(46, 3);
            this.deleteSelected_btn.Name = "deleteSelected_btn";
            this.deleteSelected_btn.Size = new System.Drawing.Size(180, 26);
            this.deleteSelected_btn.TabIndex = 7;
            this.deleteSelected_btn.Text = "Delete Selected";
            this.deleteSelected_btn.UseVisualStyleBackColor = true;
            this.deleteSelected_btn.Click += new System.EventHandler(this.deleteSelected_btn_Click);
            // 
            // createTests_layout2
            // 
            this.createTests_layout2.ColumnCount = 2;
            this.createTests_layout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.createTests_layout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.createTests_layout2.Controls.Add(this.sendTest_lbl, 0, 2);
            this.createTests_layout2.Controls.Add(this.addToTest_btn, 0, 0);
            this.createTests_layout2.Controls.Add(this.deleteQuestion_btn, 1, 0);
            this.createTests_layout2.Controls.Add(this.sendToClass_btn, 0, 7);
            this.createTests_layout2.Controls.Add(this.label1, 0, 3);
            this.createTests_layout2.Controls.Add(this.label2, 0, 5);
            this.createTests_layout2.Controls.Add(this.class_comboBox, 0, 6);
            this.createTests_layout2.Controls.Add(this.subject_comboBox, 0, 4);
            this.createTests_layout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createTests_layout2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createTests_layout2.Location = new System.Drawing.Point(3, 97);
            this.createTests_layout2.Name = "createTests_layout2";
            this.createTests_layout2.RowCount = 8;
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.createTests_layout2.Size = new System.Drawing.Size(481, 380);
            this.createTests_layout2.TabIndex = 6;
            // 
            // sendTest_lbl
            // 
            this.sendTest_lbl.AutoSize = true;
            this.createTests_layout2.SetColumnSpan(this.sendTest_lbl, 2);
            this.sendTest_lbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.sendTest_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendTest_lbl.Location = new System.Drawing.Point(3, 32);
            this.sendTest_lbl.Name = "sendTest_lbl";
            this.sendTest_lbl.Padding = new System.Windows.Forms.Padding(0, 50, 0, 0);
            this.sendTest_lbl.Size = new System.Drawing.Size(475, 66);
            this.sendTest_lbl.TabIndex = 4;
            this.sendTest_lbl.Text = "Send Test";
            // 
            // addToTest_btn
            // 
            this.addToTest_btn.AutoSize = true;
            this.addToTest_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addToTest_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.addToTest_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addToTest_btn.Location = new System.Drawing.Point(3, 3);
            this.addToTest_btn.Name = "addToTest_btn";
            this.addToTest_btn.Size = new System.Drawing.Size(234, 26);
            this.addToTest_btn.TabIndex = 0;
            this.addToTest_btn.Text = "Add Question To Test";
            this.addToTest_btn.UseVisualStyleBackColor = true;
            this.addToTest_btn.Click += new System.EventHandler(this.addToTest_btn_Click);
            // 
            // deleteQuestion_btn
            // 
            this.deleteQuestion_btn.AutoSize = true;
            this.deleteQuestion_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteQuestion_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteQuestion_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteQuestion_btn.Location = new System.Drawing.Point(243, 3);
            this.deleteQuestion_btn.Name = "deleteQuestion_btn";
            this.deleteQuestion_btn.Size = new System.Drawing.Size(235, 26);
            this.deleteQuestion_btn.TabIndex = 1;
            this.deleteQuestion_btn.Text = "Delete Question";
            this.deleteQuestion_btn.UseVisualStyleBackColor = true;
            this.deleteQuestion_btn.Click += new System.EventHandler(this.deleteQuestion_btn_Click);
            // 
            // sendToClass_btn
            // 
            this.sendToClass_btn.AutoSize = true;
            this.sendToClass_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.sendToClass_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.sendToClass_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendToClass_btn.Location = new System.Drawing.Point(3, 207);
            this.sendToClass_btn.Name = "sendToClass_btn";
            this.sendToClass_btn.Size = new System.Drawing.Size(234, 26);
            this.sendToClass_btn.TabIndex = 3;
            this.sendToClass_btn.Text = "Send To Class";
            this.sendToClass_btn.UseVisualStyleBackColor = true;
            this.sendToClass_btn.Click += new System.EventHandler(this.sendToClass_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.createTests_layout2.SetColumnSpan(this.label1, 2);
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 98);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 5, 5, 2);
            this.label1.Size = new System.Drawing.Size(475, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Subject:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.createTests_layout2.SetColumnSpan(this.label2, 2);
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 151);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 5, 5, 2);
            this.label2.Size = new System.Drawing.Size(475, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Class:";
            // 
            // class_comboBox
            // 
            this.createTests_layout2.SetColumnSpan(this.class_comboBox, 2);
            this.class_comboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.class_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.class_comboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.class_comboBox.FormattingEnabled = true;
            this.class_comboBox.Location = new System.Drawing.Point(3, 177);
            this.class_comboBox.Name = "class_comboBox";
            this.class_comboBox.Size = new System.Drawing.Size(475, 24);
            this.class_comboBox.TabIndex = 2;
            // 
            // subject_comboBox
            // 
            this.createTests_layout2.SetColumnSpan(this.subject_comboBox, 2);
            this.subject_comboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.subject_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subject_comboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subject_comboBox.FormattingEnabled = true;
            this.subject_comboBox.Location = new System.Drawing.Point(3, 124);
            this.subject_comboBox.Name = "subject_comboBox";
            this.subject_comboBox.Size = new System.Drawing.Size(475, 24);
            this.subject_comboBox.TabIndex = 1;
            this.subject_comboBox.SelectedIndexChanged += new System.EventHandler(this.subject_comboBox_SelectedIndexChanged);
            // 
            // question_lbl
            // 
            this.question_lbl.AutoSize = true;
            this.question_lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.question_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.question_lbl.Location = new System.Drawing.Point(3, 0);
            this.question_lbl.Name = "question_lbl";
            this.question_lbl.Size = new System.Drawing.Size(481, 28);
            this.question_lbl.TabIndex = 7;
            this.question_lbl.Text = "Question:";
            // 
            // createTest_layout5
            // 
            this.createTest_layout5.ColumnCount = 2;
            this.createTest_layout5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.createTest_layout5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 186F));
            this.createTest_layout5.Controls.Add(this.question_tBox, 0, 0);
            this.createTest_layout5.Controls.Add(this.createQuestion_btn, 1, 0);
            this.createTest_layout5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createTest_layout5.Location = new System.Drawing.Point(3, 31);
            this.createTest_layout5.Name = "createTest_layout5";
            this.createTest_layout5.RowCount = 1;
            this.createTest_layout5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.createTest_layout5.Size = new System.Drawing.Size(481, 30);
            this.createTest_layout5.TabIndex = 10;
            // 
            // question_tBox
            // 
            this.question_tBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.question_tBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.question_tBox.Location = new System.Drawing.Point(3, 3);
            this.question_tBox.Name = "question_tBox";
            this.question_tBox.Size = new System.Drawing.Size(289, 22);
            this.question_tBox.TabIndex = 8;
            this.question_tBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.question_tBox_KeyUp);
            // 
            // createQuestion_btn
            // 
            this.createQuestion_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.createQuestion_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createQuestion_btn.Location = new System.Drawing.Point(298, 3);
            this.createQuestion_btn.Name = "createQuestion_btn";
            this.createQuestion_btn.Size = new System.Drawing.Size(180, 23);
            this.createQuestion_btn.TabIndex = 9;
            this.createQuestion_btn.Text = "Create Question";
            this.createQuestion_btn.UseVisualStyleBackColor = true;
            this.createQuestion_btn.Click += new System.EventHandler(this.createQuestion_btn_Click);
            // 
            // questions_tbl
            // 
            this.questions_tbl.AllowUserToAddRows = false;
            this.questions_tbl.AllowUserToDeleteRows = false;
            this.questions_tbl.AllowUserToResizeColumns = false;
            this.questions_tbl.AllowUserToResizeRows = false;
            this.questions_tbl.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.questions_tbl.BackgroundColor = System.Drawing.SystemColors.Window;
            this.questions_tbl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.createTests_layout1.SetColumnSpan(this.questions_tbl, 3);
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.questions_tbl.DefaultCellStyle = dataGridViewCellStyle6;
            this.questions_tbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.questions_tbl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.questions_tbl.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.questions_tbl.Location = new System.Drawing.Point(490, 97);
            this.questions_tbl.MultiSelect = false;
            this.questions_tbl.Name = "questions_tbl";
            this.questions_tbl.RowHeadersVisible = false;
            this.questions_tbl.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.questions_tbl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.questions_tbl.Size = new System.Drawing.Size(482, 380);
            this.questions_tbl.TabIndex = 4;
            // 
            // testDeadline_dtPicker
            // 
            this.createTests_layout1.SetColumnSpan(this.testDeadline_dtPicker, 2);
            this.testDeadline_dtPicker.CustomFormat = " yyyy MM dd";
            this.testDeadline_dtPicker.Dock = System.Windows.Forms.DockStyle.Top;
            this.testDeadline_dtPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testDeadline_dtPicker.Location = new System.Drawing.Point(685, 3);
            this.testDeadline_dtPicker.MinDate = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            this.testDeadline_dtPicker.Name = "testDeadline_dtPicker";
            this.testDeadline_dtPicker.Size = new System.Drawing.Size(287, 22);
            this.testDeadline_dtPicker.TabIndex = 3;
            // 
            // createTest_btn
            // 
            this.createTest_btn.AutoSize = true;
            this.createTest_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.createTest_btn.Dock = System.Windows.Forms.DockStyle.Top;
            this.createTest_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createTest_btn.Location = new System.Drawing.Point(880, 31);
            this.createTest_btn.Name = "createTest_btn";
            this.createTest_btn.Size = new System.Drawing.Size(92, 26);
            this.createTest_btn.TabIndex = 2;
            this.createTest_btn.Text = "Create Test";
            this.createTest_btn.UseVisualStyleBackColor = true;
            this.createTest_btn.Click += new System.EventHandler(this.createTest_btn_Click);
            // 
            // questions_comboBox
            // 
            this.questions_comboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.questions_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.questions_comboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questions_comboBox.FormattingEnabled = true;
            this.questions_comboBox.Location = new System.Drawing.Point(3, 67);
            this.questions_comboBox.Name = "questions_comboBox";
            this.questions_comboBox.Size = new System.Drawing.Size(481, 24);
            this.questions_comboBox.TabIndex = 9;
            this.questions_comboBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.questions_comboBox_KeyUp);
            // 
            // testname_tBox
            // 
            this.createTests_layout1.SetColumnSpan(this.testname_tBox, 2);
            this.testname_tBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.testname_tBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testname_tBox.Location = new System.Drawing.Point(490, 31);
            this.testname_tBox.Name = "testname_tBox";
            this.testname_tBox.Size = new System.Drawing.Size(384, 22);
            this.testname_tBox.TabIndex = 1;
            this.testname_tBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.testname_tBox_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(490, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "Test:";
            // 
            // lecturer_tabCtr
            // 
            this.lecturer_tabCtr.Controls.Add(this.createTests_tab);
            this.lecturer_tabCtr.Controls.Add(this.verifyAnswers_tab);
            this.lecturer_tabCtr.Controls.Add(this.announce_Tab);
            this.lecturer_tabCtr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lecturer_tabCtr.ItemSize = new System.Drawing.Size(42, 18);
            this.lecturer_tabCtr.Location = new System.Drawing.Point(0, 0);
            this.lecturer_tabCtr.Multiline = true;
            this.lecturer_tabCtr.Name = "lecturer_tabCtr";
            this.lecturer_tabCtr.SelectedIndex = 0;
            this.lecturer_tabCtr.Size = new System.Drawing.Size(989, 595);
            this.lecturer_tabCtr.TabIndex = 0;
            // 
            // LecturerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 595);
            this.Controls.Add(this.lecturer_tabCtr);
            this.Name = "LecturerForm";
            this.Text = "Lecturer:";
            this.Load += new System.EventHandler(this.LecturerForm_Load);
            this.announce_Tab.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.verifyAnswers_tab.ResumeLayout(false);
            this.verifyTests_layout1.ResumeLayout(false);
            this.verifyTests_layout1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.verification_grid)).EndInit();
            this.verifyTests_layout2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.createTests_tab.ResumeLayout(false);
            this.createTests_layout1.ResumeLayout(false);
            this.createTests_layout1.PerformLayout();
            this.createTests_layout4.ResumeLayout(false);
            this.createTests_layout4.PerformLayout();
            this.createTests_layout2.ResumeLayout(false);
            this.createTests_layout2.PerformLayout();
            this.createTest_layout5.ResumeLayout(false);
            this.createTest_layout5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questions_tbl)).EndInit();
            this.lecturer_tabCtr.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage announce_Tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox announce_text;
        private System.Windows.Forms.Button announce_create_btn;
        private System.Windows.Forms.Label announcement_lbl;
        private System.Windows.Forms.TabPage verifyAnswers_tab;
        private System.Windows.Forms.TableLayoutPanel verifyTests_layout1;
        private System.Windows.Forms.DataGridView verification_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnswerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnswerText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsCorrect;
        private System.Windows.Forms.DataGridViewTextBoxColumn Points;
        private System.Windows.Forms.TableLayoutPanel verifyTests_layout2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button verify_btn;
        private System.Windows.Forms.Label question_lbl2;
        private System.Windows.Forms.Label test_lbl2;
        private System.Windows.Forms.Label subject_lbl2;
        private System.Windows.Forms.Label class_lbl2;
        private System.Windows.Forms.ComboBox class_comboBox2;
        private System.Windows.Forms.ComboBox subject_comboBox2;
        private System.Windows.Forms.ComboBox test_comboBox2;
        private System.Windows.Forms.ComboBox question_comboBox2;
        private System.Windows.Forms.Label answers_lbl;
        private System.Windows.Forms.TabPage createTests_tab;
        private System.Windows.Forms.TableLayoutPanel createTests_layout1;
        private System.Windows.Forms.ComboBox tests_comboBox;
        private System.Windows.Forms.TableLayoutPanel createTests_layout4;
        private System.Windows.Forms.Button deleteTest_btn;
        private System.Windows.Forms.Button deleteSelected_btn;
        private System.Windows.Forms.TableLayoutPanel createTests_layout2;
        private System.Windows.Forms.Label sendTest_lbl;
        private System.Windows.Forms.Button addToTest_btn;
        private System.Windows.Forms.Button deleteQuestion_btn;
        private System.Windows.Forms.Button sendToClass_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox class_comboBox;
        private System.Windows.Forms.ComboBox subject_comboBox;
        private System.Windows.Forms.Label question_lbl;
        private System.Windows.Forms.TableLayoutPanel createTest_layout5;
        private System.Windows.Forms.TextBox question_tBox;
        private System.Windows.Forms.Button createQuestion_btn;
        private System.Windows.Forms.DataGridView questions_tbl;
        private System.Windows.Forms.DateTimePicker testDeadline_dtPicker;
        private System.Windows.Forms.Button createTest_btn;
        private System.Windows.Forms.ComboBox questions_comboBox;
        private System.Windows.Forms.TextBox testname_tBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl lecturer_tabCtr;
        private System.Windows.Forms.Label announcement_list_lbl;
        private System.Windows.Forms.ListBox announcement_list;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label announce_msg_lbl;
        private System.Windows.Forms.TextBox announce_edit_msg_text;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button announce_update_btn;
        private System.Windows.Forms.Button announce_remove_btn;
        private System.Windows.Forms.Button announce_edit_btn;
        private System.Windows.Forms.Button announce_edit_cancel_btn;


    }
}