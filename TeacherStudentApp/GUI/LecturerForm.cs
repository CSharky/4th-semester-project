﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.GUI
{
    public partial class LecturerForm : Form
    {
        public LecturerForm()
        {
            InitializeComponent();
        }

        private void subject_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void createTest_btn_Click(object sender, EventArgs e)
        {
            string className = class_comboBox.SelectedText;
            string subjectName = subject_comboBox.SelectedText;

            string testName = testname_tBox.Text;
            TestController tc = new TestController();
            TeacherController tcc = new TeacherController();
            Teacher teacher = tcc.GetTeacher(Global.AUTHENTICATED_USER.UserId);
            tc.InsertTest(testName, teacher.TeacherId);
            UpdateTestsDropDown();

        }

        private void deleteTest_btn_Click(object sender, EventArgs e)
        {
            Test t = (Test)tests_comboBox.SelectedItem;
            TestController tc = new TestController();

            tc.DeleteTest(t.TestId);
            UpdateTestsDropDown();
            UpdateTable();
        }

        private void createQuestion_btn_Click(object sender, EventArgs e)
        {
            string question = question_tBox.Text;

            QuestionController qc = new QuestionController();

            qc.InsertQuestion(question);
            UpdateQuestionDropDown();
            question_tBox.Text = "";
            this.ActiveControl = question_tBox;
        }

        private void deleteQuestion_btn_Click(object sender, EventArgs e)
        {
            Question question = (Question)questions_comboBox.SelectedItem;
            int questionId = question.QuestionId;

            QuestionController qc = new QuestionController();

            qc.DeleteQuestion(questionId);
            UpdateQuestionDropDown();
        }

        private void LecturerForm_Load(object sender, EventArgs e)
        {
            UpdateTestsDropDown();
            UpdateQuestionDropDown();
            SClassController sc = new SClassController();
            class_comboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            class_comboBox.DataSource = sc.GetDropDownClasses();
            class_comboBox.DisplayMember = "DisplayName";

            TopicController topicC = new TopicController();
            subject_comboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            subject_comboBox.DataSource = topicC.GetDropDownTopics();
            subject_comboBox.DisplayMember = "DisplayName";
            LoadVerifyAnswersTab();

            AnnouncementController ac = new AnnouncementController();

            TeacherController tc = new TeacherController();
            Teacher teacher = tc.GetTeacher(Global.AUTHENTICATED_USER.UserId);

            announcement_list.Items.AddRange(ac.GetAllAnnouncementsForTeacher(teacher.TeacherId).ToArray());
        }
        private void UpdateTestsDropDown()
        {
            tests_comboBox.DataSource = null;
            TestController tc = new TestController();
            tests_comboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            TeacherController tcc = new TeacherController();
            Teacher t = tcc.GetTeacher(Global.AUTHENTICATED_USER.UserId);
            tests_comboBox.DataSource = tc.GetDropDownTests(t.TeacherId);
            tests_comboBox.DisplayMember = "DisplayName";
        }

        private void UpdateQuestionDropDown()
        {
            question_comboBox2.DataSource = null;
            QuestionController qc = new QuestionController();
            questions_comboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            questions_comboBox.DataSource = qc.GetDropDownQuestions();
            questions_comboBox.DisplayMember = "DisplayName";
        }

        private void tests_comboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                UpdateTable();
            }
        }

        private void saveTest_btn_Click(object sender, EventArgs e)
        {
        }
        private void UpdateTable()
        {
            //ComboBox cb = (ComboBox)sender;
            questions_tbl.DataSource = null;
            ComboBox cb = tests_comboBox;
            Test test = (Test)cb.SelectedItem;
            if (test != null)
            {
                TestController tc = new TestController();
                questions_tbl.AutoGenerateColumns = true;
                var list = tc.GetTestQuestionsForDataset(test.TestId);
                questions_tbl.DataSource = list;
                foreach (DataGridViewColumn col in questions_tbl.Columns)
                {
                    col.Visible = false;
                }

                try
                {
                    questions_tbl.Columns["DisplayName"].Visible = true;
                    questions_tbl.Columns["DisplayName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                catch (Exception)
                {
                    //the column is not there
                }
            }
        }

        private void deleteSelected_btn_Click(object sender, EventArgs e)
        {
            if (sender != null)
            {
                Question q = (Question)questions_tbl.SelectedRows[0].DataBoundItem;
                Test t = (Test)tests_comboBox.SelectedItem;

                TestQuestionController tqc = new TestQuestionController();
                tqc.DeleteTestQuestion(tqc.GetSingleTestQuestion(t.TestId, q.QuestionId).TestQuestionId);
                UpdateTable();
            }

        }

        private void addToTest_btn_Click(object sender, EventArgs e)
        {
            Question q = (Question)questions_comboBox.SelectedItem;
            TestController tc = new TestController();
            Test t = (Test)tests_comboBox.SelectedItem;
            tc.AddQuestionToTest(t.TestId, q.QuestionId);
            UpdateTable();
        }

        private void sendToClass_btn_Click(object sender, EventArgs e)
        {
            TestController tc = new TestController();
            Test t = (Test)tests_comboBox.SelectedItem;
            SchoolClass sc = (SchoolClass)class_comboBox.SelectedItem;
            Topic topic = (Topic)subject_comboBox.SelectedItem;
            tc.SendToClass(t, sc, topic, testDeadline_dtPicker.Value);
        }

        private void question_tBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != null && sender.Equals(question_tBox))
            {
                if (e.KeyData == Keys.Enter)
                    createQuestion_btn.PerformClick();
            }
        }

        private void testname_tBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != null && sender.Equals(testname_tBox))
            {
                if (e.KeyData == Keys.Enter)
                    createTest_btn.PerformClick();
            }
        }

        private void questions_comboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != null && sender.Equals(questions_comboBox))
            {
                if (e.KeyData == Keys.Enter)
                    addToTest_btn.PerformClick();
            }
        }

        private void announce_create_btn_Click(object sender, EventArgs e)
        {
            if (announce_text.Text != null && announce_text.Text != String.Empty)
            {
                string announcement = announce_text.Text;

                AnnouncementController ac = new AnnouncementController();
                TeacherController tc = new TeacherController();

                Teacher teacher = tc.GetTeacher(Global.AUTHENTICATED_USER.UserId);

                ac.InsertAnnouncement(announcement, teacher.TeacherId);
                RefreshAnnounceList();
            }
            else
            {
                MessageBox.Show("Please input a message first!");
            }
        }

        private void announcement_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (announcement_list.SelectedItem != null)
            {
                string curItem = announcement_list.SelectedItem.ToString();

                AnnouncementController ac = new AnnouncementController();
                TeacherController tc = new TeacherController();

                var announcement = ac.GetAnnouncement(announcement_list.SelectedItem.ToString());
                var teacher = tc.GetTeacher(announcement.TeacherId);

                announce_edit_msg_text.Text = announcement.Message;
            }
            else
            {
                MessageBox.Show("Please select a item to show more details!");
            }
        }

        private void announce_edit_btn_Click(object sender, EventArgs e)
        {
            announce_edit_msg_text.Enabled = true;
            announce_update_btn.Enabled = true;
            announce_edit_btn.Enabled = false;
        }

        private void announce_remove_btn_Click(object sender, EventArgs e)
        {
            AnnouncementController ac = new AnnouncementController();
            var announcement = ac.GetAnnouncement(announcement_list.SelectedItem.ToString());

            ac.DeleteAnnouncement(announcement.AnnouncementId);

            RefreshAnnounceList();
        }

        private void announce_update_btn_Click(object sender, EventArgs e)
        {
            announce_edit_msg_text.Enabled = false;
            announce_update_btn.Enabled = false;
            announce_edit_btn.Enabled = true;

            AnnouncementController ac = new AnnouncementController();
            var announcement = ac.GetAnnouncement(announcement_list.SelectedItem.ToString());

            ac.UpdateAnnouncement(announcement.AnnouncementId, announce_edit_msg_text.Text);

            RefreshAnnounceList();
        }

        private void announce_edit_cancel_btn_Click(object sender, EventArgs e)
        {
            announce_edit_msg_text.Enabled = false;
            announce_update_btn.Enabled = false;
            announce_edit_btn.Enabled = true;
            announce_edit_msg_text.Clear();

            RefreshAnnounceList();
        }

        private void RefreshAnnounceList()
        {
            AnnouncementController ac = new AnnouncementController();

            TeacherController tc = new TeacherController();
            Teacher teacher = tc.GetTeacher(Global.AUTHENTICATED_USER.UserId);

            announcement_list.Items.Clear();
            announcement_list.Items.AddRange(ac.GetAllAnnouncementsForTeacher(teacher.TeacherId).ToArray());
        }
    }
}
