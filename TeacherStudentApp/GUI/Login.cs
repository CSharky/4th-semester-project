﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;
using TeacherStudentApp.GUI;

namespace TeacherStudentApp
{
    public partial class Login : Form
    {
        private string USER_RIGHTS_STUDENT = Global.USER_RIGHTS_STUDENT;
        private string USER_RIGHTS_LECTURER = Global.USER_RIGHTS_LECTURER;
        private string USER_RIGHTS_ADMIN = Global.USER_RIGHTS_ADMIN;
        public Login()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            UserController uc = new UserController();
            User user = uc.GetAuthenticatedUser(userTxt.Text, passTxt.Text);
            if (user != null && (user.Rights == USER_RIGHTS_LECTURER || user.Rights == USER_RIGHTS_ADMIN))
            {
                this.Hide();
                uc.RememberUser(user);
                LecturerForm lecturer = new LecturerForm();
                TeacherController tc = new TeacherController();
                Teacher teacher = tc.GetTeacher(Global.AUTHENTICATED_USER.UserId);
                lecturer.Text = "Lecturer: " + teacher.Fname + " " + teacher.Lname;
                lecturer.ShowDialog();
                this.Close();
            }
            else if (user != null && (user.Rights == USER_RIGHTS_STUDENT))
            {

                this.Hide();
                uc.RememberUser(user);
                StudentForm student = new StudentForm();
                StudentController sc = new StudentController();
                Student stu = sc.GetStudent(Global.AUTHENTICATED_USER.UserId);
                student.Text = "Student: " + stu.Fname + " " + stu.Lname;
                student.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("You are not registered in the system or you entered a wrong password");
                resetPW_btn.Visible = true;
            }
            //RememberUser(user);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserController uc = new UserController();
            uc.CreateUser(userTxt.Text, passTxt.Text);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            UserController uc = new UserController();
            if (uc.GetAllUsers().Count == 0)
            {
                registerBtn.Visible = true;
            }
        }

        private void resetPW_btn_Click(object sender, EventArgs e)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                UserController uc = new UserController();
                MessageBox.Show("The new password will be sent to the user in the username field!");
                if (userTxt.Text != "")
                {
                    User user = db.Users.SingleOrDefault(u => u.Username == userTxt.Text);
                    if (user != null)
                    {
                        uc.ResetPassword(userTxt.Text);
                    }
                }
                else
                    MessageBox.Show("Please write in your username in the Username field!");

            }
        }
    }
}
