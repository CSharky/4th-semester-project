﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeacherStudentApp.CTR;
using TeacherStudentApp.DAL;
using TeacherStudentApp.GUI;

namespace TeacherStudentApp
{
    public partial class ChangePasswordForm : Form
    {
        public ChangePasswordForm()
        {
            InitializeComponent();
        }

        private void acceptBtn_Click(object sender, EventArgs e)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                User user = db.Users.SingleOrDefault(u => u.UserId == Global.AUTHENTICATED_USER.UserId);
                UserController uc = new UserController();
                if (user != null)
                {
                    if (uc.CheckOldPassword(user, oldPasswordTxt.Text))
                    {
                        if (oldPasswordTxt.Text.Equals(newPassTxt.Text))
                        {
                            MessageBox.Show("Your old password must be different from the new one!");
                        }
                        else
                        {
                            if (newPassTxt.Text.Equals(newPassVerTxt.Text))
                            {
                                uc.ChangePassword(user, newPassTxt.Text);
                                MessageBox.Show("Your password has been changed!");
                                this.Close();
                            }
                            else
                                MessageBox.Show("Please, enter the same password in the last two boxes!");
                        }
                    }
                    else
                        MessageBox.Show("The old password is incorrect!");
                }
            }
        }
    }
}
