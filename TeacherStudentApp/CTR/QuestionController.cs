﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class QuestionController
    {

        private bool IsNullOrEmpty(string value)
        {
            return (value == null || value == String.Empty || value.Trim() == String.Empty);
        }

        public Question GetSingleQuestion(int questionId)
        {
            using(var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    Question qq = db.Questions.SingleOrDefault(q => q.QuestionId == questionId);
                    return qq;
                }
                catch (Exception)
                {
                    return null;
                } 
            }
        }

        public Question InsertQuestion(String questionText)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                if (!IsNullOrEmpty(questionText))
                {
                    Question question = new Question();
                    question.QuestionText = questionText;

                    try
                    {
                        db.Questions.InsertOnSubmit(question);
                        db.SubmitChanges();
                        return GetSingleQuestion(question.QuestionId);
                    }
                    catch (Exception)
                    {
                        return null;
                    }//The try-catch ends.

                }
                else
                {
                    return null;
                }//First if ends.
            }
        }

        public void DeleteQuestion(int questionId)
        {
            Question question = GetSingleQuestion(questionId);
            using (var db = new TeacherStudentDBDataContext())
            {
                if (question != null)
                {
                    try
                    {
                        var qq = db.Questions.SingleOrDefault(q => q.QuestionId == questionId);
                        db.Questions.DeleteOnSubmit(qq);
                        db.SubmitChanges();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public List<Question> GetAllQuestions()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Questions.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public IList<Question> GetDropDownQuestions()
        {
            return GetAllQuestions().OrderByDescending(o => o.QuestionId).ToList();
        }

    }

}
