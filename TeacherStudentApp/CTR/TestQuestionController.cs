﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class TestQuestionController
    {

        private TeacherStudentDBDataContext db = new TeacherStudentDBDataContext();

        private bool IsNullOrEmpty(string value)
        {
            return (value == null || value == String.Empty);
        }

        public TestQuestion GetSingleTestQuestion(int testQuestionId)
        {
            try
            {
                TestQuestion tqq = db.TestQuestions.SingleOrDefault(tq => tq.TestQuestionId == testQuestionId);
                return tqq;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public TestQuestion GetSingleTestQuestion(int testId, int questionId)
        {
            try
            {
                return db.TestQuestions.FirstOrDefault(tq => tq.TestId == testId && tq.QuestionId == questionId);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public TestQuestion InsertTestQuestion(int testId, int questionId)
        {
            TestQuestion testQuestion = new TestQuestion();
            testQuestion.TestId = testId;
            testQuestion.QuestionId = questionId;

            try
            {
                var theTq = db.TestQuestions.FirstOrDefault(tq => tq.QuestionId == testQuestion.QuestionId && tq.TestId == testId);
                if (theTq == null)
                {
                    db.TestQuestions.InsertOnSubmit(testQuestion);
                    db.SubmitChanges();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("TestQuestion was not added to the test because it was already added before");
                }
                return GetSingleTestQuestion(testQuestion.TestQuestionId);
            }
            catch (Exception)
            {
                return null;
            }//The try-catch ends.

        }

        public void DeleteTestQuestion(int testQuestionId)
        {
            TestQuestion testQuestion = GetSingleTestQuestion(testQuestionId);

            if (testQuestion != null)
            {
                try
                {

                    db.TestQuestions.DeleteOnSubmit(testQuestion);
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void DeleteTestQuestionFromDBAndTests(int testQuestionId)
        {
            TestQuestion testQuestion = GetSingleTestQuestion(testQuestionId);
            QuestionController qctrl = new QuestionController();

            if (testQuestion != null)
            {
                try
                {
                    var tq = db.TestQuestions.SingleOrDefault(t => t.TestQuestionId == testQuestion.TestQuestionId);
                    db.TestQuestions.DeleteOnSubmit(tq);
                    qctrl.DeleteQuestion(testQuestion.QuestionId);
                    db.SubmitChanges();

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }



    }
}
