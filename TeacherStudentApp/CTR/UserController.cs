﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class UserController
    {
        public void RememberUser(User user)
        {
            UserController uc = new UserController();
            Global.AUTHENTICATED_USER = user;
        }

        public IList<User> GetAllUsers()
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                return db.Users.ToList();
            }
        }
        private User CreateUser(User user)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    User usr = GetUser(user.Username);
                    if (usr == null)
                    {
                        db.Users.InsertOnSubmit(user);
                        db.SubmitChanges();
                        return user;
                    }
                    else
                        return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        private User CreateUser(string username, string password, string salt, string rights)
        {
            User user = new User();
            user.Username = username;
            user.Password = CalculateMD5Hash(password + salt); //TODO hash this shit + add salt to it
            user.Salt = salt;
            user.Rights = rights;
            return CreateUser(user);
        }
        public User CreateUser(string username, string password, string rights)
        {
            string salt = GenerateSalt();
            return CreateUser(username, password, salt, rights);
        }
        public User CreateUser(string username, string password)
        {
            string rights = "student";
            return CreateUser(username, password, rights);
        }
        private string GenerateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[17];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }
        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        public User GetAuthenticatedUser(string username, string password)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    var usr = db.Users.SingleOrDefault(u => u.Username.Equals(username));
                    string pass = CalculateMD5Hash(password + usr.Salt);
                    if (usr.Password == pass)
                    {
                        return (User)usr;
                    }
                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        public User GetUser(string username)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Users.SingleOrDefault(u => u.Username.Equals(username));
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public User ChangePassword(User user, string password)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                var userChange = db.Users.SingleOrDefault(u => u.Username == user.Username);
                if (userChange != null)
                {
                    userChange.Salt = GenerateSalt();
                    userChange.Password = CalculateMD5Hash(password + userChange.Salt);
                    db.SubmitChanges();
                    return user;
                }
                return null;
            }
        }
        public bool CheckOldPassword(User user, string password)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                string newPass = CalculateMD5Hash(password + user.Salt);
                if (user.Password.Equals(newPass))
                    return true;
                else
                    return false;
            }
        }
        private string GeneratePassword()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[10];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }
        public void ResetPassword(string username)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                var user = db.Users.SingleOrDefault(u => u.Username == username);
                if (user != null)
                {
                    string newPass = GeneratePassword();
                    string salt = GenerateSalt();
                    user.Password = CalculateMD5Hash(newPass + salt);
                    user.Salt = salt;
                    SendResetEmail(username, newPass);
                    db.SubmitChanges();
                }
            }

        }
        private void SendResetEmail(string username, string newPass)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    User user = db.Users.SingleOrDefault(u => u.Username == username);
                    if (user != null)
                    {
                        Student student = db.Students.SingleOrDefault(s => s.UserId == user.UserId);

                        if (student != null)
                        {
                            mail.From = new MailAddress("SENDER EMAIL ADDRESS");
                            mail.To.Add(student.Email.ToString());
                            mail.Subject = "Password Reset";
                            mail.Body = "Password Reset Automatic Email \r\n Please change your new password as soon as possible! \r\n Your new password is: " + newPass;
                            //Attachment attachment = new Attachment(filename);
                            //mail.Attachments.Add(attachment);

                            SmtpServer.Port = 25;
                            SmtpServer.Credentials = new System.Net.NetworkCredential("SENDERUSERNAME", "SENDERPASSWORD");
                            SmtpServer.EnableSsl = true;

                            SmtpServer.Send(mail);
                        }
                    }

                }
                catch (Exception)
                {
                    //TODO: Check this
                    throw;
                }
            }
        }


        public void DeleteUser(string username)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    User u = db.Users.SingleOrDefault(us => us.Username.Equals(username));
                    if (u != null)
                    {
                        db.Users.DeleteOnSubmit(u);
                        db.SubmitChanges();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
