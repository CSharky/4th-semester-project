﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    class TeachesController
    {
        public Teach GetTeach(int teacherId, int schoolClassId, int topicId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                return db.Teaches.SingleOrDefault(t => t.TeacherId == teacherId && t.SchoolClassId == schoolClassId && t.TopicId == topicId);
            }
        }
        public IList<Topic> GetTopicsForClass(int schoolClassId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<Topic> theList = new List<Topic>();
                var list =  db.Teaches.Where(t => t.SchoolClassId == schoolClassId);
                foreach (Teach item in list)
                {
                    theList.Add(item.Topic);
                }
                return theList;
            }
        }
        public IList<SchoolClass> GetClassesForTopic(int topicId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<SchoolClass> theList = new List<SchoolClass>();
                var list = db.Teaches.Where(t => t.TopicId == topicId);
                foreach (Teach item in list)
                {
                    theList.Add(item.SchoolClass);
                }
                return theList;
            }
        }
        public IList<SchoolClass> GetClassesForTeacher(int teacherId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<SchoolClass> theList = new List<SchoolClass>();
                var list = db.Teaches.Where(t => t.TeacherId == teacherId);
                foreach (Teach item in list)
                {
                    theList.Add(item.SchoolClass);
                }
                return theList;
            }
        }
        public Teach InsertTeach(int teacherId, int schoolClassId, int topicId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                Teach t = new Teach();
                t.TeacherId = teacherId;
                t.SchoolClassId = schoolClassId;
                t.TopicId = topicId;
                db.Teaches.InsertOnSubmit(t);
                db.SubmitChanges();
                return t;
            }
        }
    }

}
