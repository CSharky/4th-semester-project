﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class TeacherController
				{
						#region Create Teacher Methods.
						public Teacher CreateTeacher(string firstName, string lastName, string email, string username, string password)
						{
								Teacher teacher = new Teacher();
								teacher.Fname = firstName;
								teacher.Lname = lastName;
								teacher.Email = email;
								User user = new User();
								user.Username = username;
								user.Password = password;

								return CreateTeacher(teacher, user);
						}

						private Teacher CreateTeacher(Teacher teacher, User user)
						{
								using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
								{
										UserController uc = new UserController();
										user = uc.CreateUser(user.Username, user.Password);
										if (user != null)
										{
												teacher.UserId = user.UserId;
												db.Teachers.InsertOnSubmit(teacher);
												try
												{
														db.SubmitChanges();
												}
												catch (Exception)
												{
														uc.DeleteUser(user.Username);
												}
												return GetTeacher(user.UserId);
										}
										else
										{
												return null;
										}
								}
						}
						#endregion

						#region Get Teacher Methods.
						public Teacher GetTeacher(string email)
						{
								return GetTeacher(email, -1);
						}
						public Teacher GetTeacher(int userId)
						{
								return GetTeacher("-1", userId);
						}

						private Teacher GetTeacher(string email, int userId)
						{
								using (var db = new TeacherStudentDBDataContext())
								{
										return db.Teachers.SingleOrDefault(s => s.Email.ToLower().Equals(email.ToLower()) || s.UserId == userId);
								}
						}
						#endregion

						//If the User instance gets deleted then Teacher will be deleted also.
				}
}
