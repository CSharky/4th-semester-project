﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class AnnouncementController
    {
        public Announcement InsertAnnouncement(string message, int teacherId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                if (!IsNullOrEmpty(message))
                {
                    Announcement annaounce = new Announcement();
                    annaounce.Message = message;
                    annaounce.TeacherId = teacherId;
                    try
                    {
                        db.Announcements.InsertOnSubmit(annaounce);
                        db.SubmitChanges();
                        return GetAnnouncement(annaounce.AnnouncementId);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public Announcement UpdateAnnouncement(int announcementId, string message)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                Announcement announce = GetAnnouncement(announcementId);

                if (announce != null)
                {
                    if (!IsNullOrEmpty(message))
                    {
                        var theAnnouncement = db.Announcements.SingleOrDefault(a => a.AnnouncementId == announcementId);
                        theAnnouncement.Message = message;

                        try
                        {
                            db.SubmitChanges();
                            return GetAnnouncement(announce.AnnouncementId);
                        }
                        catch (Exception e)
                        {
                            e.Message.ToString();
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public void DeleteAnnouncement(int announcementId)
        {
            Announcement announce = GetAnnouncement(announcementId);
            if (announce != null)
            {
                using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
                {
                    try
                    {
                        var ann = db.Announcements.SingleOrDefault(a => a.AnnouncementId == announcementId);
                        db.Announcements.DeleteOnSubmit(ann);
                        db.SubmitChanges();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }
        }

        public Announcement GetAnnouncement(int announcementId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Announcements.SingleOrDefault(a => a.AnnouncementId == announcementId);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public Announcement GetAnnouncement(string message)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Announcements.SingleOrDefault(a => a.Message == message);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<Announcement> GetAllAnnouncements()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Announcements.ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<String> GetAllAnnouncementsForTeacher(int teacherId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    List<String> theList = new List<String>();
                        var anotherList = db.Announcements.Where(a => a.TeacherId == teacherId);
                        foreach (Announcement item in anotherList)
                        {
                            theList.Add(item.Message.ToString());
                        }
                    return theList;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<String> GetAllAnnouncementsForClass(int schoolClassId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    List<String> theList = new List<String>();
                    var list = db.Teaches.Where(t => t.SchoolClassId == schoolClassId);
                    foreach (Teach item in list)
                    {
                        var anotherList = db.Announcements.Where(a => a.TeacherId == item.TeacherId);
                        foreach (Announcement item2 in anotherList)
                        {
                            theList.Add(item2.Message.ToString());
                        }
                    }
                    return theList;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        private bool IsNullOrEmpty(string value)
        {
            return (value == null || value == String.Empty);
        }
    }
}
