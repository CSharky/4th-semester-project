﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;
using TeacherStudentApp.CTR;

namespace TeacherStudentApp.CTR
{
    public class StudentController
    {
        private Student CreateStudent(Student student, User user)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                UserController uc = new UserController();
                user = uc.CreateUser(user.Username, user.Password);
                if (user != null)
                {
                    student.UserId = user.UserId;
                    db.Students.InsertOnSubmit(student);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (Exception)
                    {
                        uc.DeleteUser(user.Username);
                    }
                    return GetStudent(user.UserId);
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// This should be called only when you know the SchoolClassId
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="schoolClassId">Needs to be known before calling this method. Try getting it with SClassController method</param>
        /// <returns></returns>
        public Student CreateStudent(string firstName, string lastName, string email, int schoolClassId, string username, string password)
        {
            Student student = new Student();
            student.Fname = firstName;
            student.Lname = lastName;
            student.Email = email;
            student.SchoolClassId = schoolClassId;
            User user = new User();
            user.Username = username;
            user.Password = password;

            return CreateStudent(student, user);
        }
        private Student GetStudent(string email, int userId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                return db.Students.SingleOrDefault(s => s.Email.ToLower().Equals(email.ToLower()) || s.UserId == userId);
            }
        }
        public Student GetStudent(string email)
        {
            return GetStudent(email, -1);
        }
        public Student GetStudent(int userId)
        {
            return GetStudent("-1", userId);
        }
        private void DeleteStudent(string email, int userId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                }
                catch (Exception ex)
                {
                    throw new Exception("There are more than one students with that email or userId");
                }
            }
        }
        public void DeleteStudent(string email)
        {
            DeleteStudent(email, -1);
        }
        public void DeleteStudent(int userId)
        {
            DeleteStudent("-1", userId);
        }

        public int GetStudentPoints(int studentId)
        {
            int studentPoints = -1;

            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                Student st = db.Students.SingleOrDefault(s => s.StudentId == studentId);
                if (st != null)
                    studentPoints = st.Score;
            }
            return studentPoints;
        }

        public IList<StudentWithClassName> GetTop10Students()
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var top10 = (from s in db.Students
                             join c in db.SchoolClasses
                                             on s.SchoolClassId equals c.SchoolClassId
                             orderby s.Score descending

                             select new StudentWithClassName()
                             {
                                 Rank = 0,
                                 FirstName = s.Fname,
                                 LastName = s.Lname,
                                 ClassName = c.SchoolClassName,
                                 Score = s.Score

                             }).Take(10).ToList();
                return top10;
            }
        }

        public void AddPoints(int studentId, int points)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var query =
                                from s in db.Students
                                where s.StudentId == studentId
                                select s;

                foreach (Student s in query)
                {
                    s.Score += points;
                }

                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                }
            }


            #region why this doesn't work? TODO Investigate the issue.
            // this doesn't work because is not correct code :)

            //Student student = GetStudentToAddPoints(studentId);

            //if (student != null)
            //{
            //		using (var db = new TeacherStudentDBDataContext())
            //		{
            //				db.Log = new System.IO.StreamWriter("C:\\linq.log") { AutoFlush = true };
            //				student.Score += points;
            //				string scoreString = student.Score.ToString();
            //				try
            //				{
            //						db.SubmitChanges();

            //				}
            //				catch (Exception)
            //				{
            //				}
            //		}
            //}
            #endregion
        }

        public void UploadPicture(int studentId, string base64string)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var student = db.Students.SingleOrDefault(s => s.StudentId == studentId);
                student.Picture64 = base64string;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
        }
        public string GetPicture(int studentId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var student = db.Students.SingleOrDefault(s => s.StudentId == studentId);
                return student.Picture64;
                
            }
        }
        //private Student GetStudentToAddPoints(int studentId)
        //{
        //		using (var db = new TeacherStudentDBDataContext())
        //		{
        //				try
        //				{
        //						return db.Students.SingleOrDefault(s => s.StudentId == studentId);
        //				}
        //				catch (Exception)
        //				{
        //						return null;
        //				}

        //		}
        //}
    }
}
