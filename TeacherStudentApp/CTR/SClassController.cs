﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    class SClassController
    {
        public List<SchoolClass> GetAllClasses()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.SchoolClasses.ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public IList<SchoolClass > GetDropDownClasses()
        {
            return GetAllClasses();
        }

        public IList<SchoolClass> GetDropDownClassesForStudent()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                IList<SchoolClass> scList = new List<SchoolClass>();

                try
                {
                    StudentController sc = new StudentController();
                    Student student = sc.GetStudent(Global.AUTHENTICATED_USER.UserId);
                    scList.Add(db.SchoolClasses.SingleOrDefault(c=>c.SchoolClassId==student.SchoolClassId));
                    return scList;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

    }
}
