﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    public class TestController
    {
        public Test InsertTest(string testName, int? teacherId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                //There should be a valid name for the test.
                if (!IsNullOrEmpty(testName))
                {
                    Test test = new Test();
                    test.TestName = testName;
                    test.TeacherId = teacherId;
                    try
                    {
                        db.Tests.InsertOnSubmit(test);
                        db.SubmitChanges();
                        return GetTest(test.TestId);
                    }
                    catch (Exception)
                    {
                        return null;
                    }//The try-catch ends.

                }
                else
                {
                    return null;
                }//First if ends.
            }
        }
        public Test InsertTest(string testName)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                //There should be a valid name for the test.
                if (!IsNullOrEmpty(testName))
                {
                    Test test = new Test();
                    test.TestName = testName;
                    try
                    {
                        db.Tests.InsertOnSubmit(test);
                        db.SubmitChanges();
                        return GetTest(test.TestId);
                    }
                    catch (Exception)
                    {
                        return null;
                    }//The try-catch ends.

                }
                else
                {
                    return null;
                }//First if ends.
            }
        }

        public Test UpdateTest(int testId, string testName)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                Test test = GetTest(testId);

                if (test != null)
                {
                    //There should be a valid name for the test.
                    if (!IsNullOrEmpty(testName))
                    {
                        var theTest = db.Tests.SingleOrDefault(t => t.TestId == testId);
                        theTest.TestName = testName;
                        //TO DO the questions list must be saved to TestQuestion.

                        try
                        {
                            db.SubmitChanges();
                            return GetTest(test.TestId);
                        }
                        catch (Exception e)
                        {
                            e.Message.ToString();
                            return null;
                        }//The try-catch ends.

                    }
                    else
                    {
                        return null;
                    }//Second if ends.

                }
                else
                {
                    return null;
                }//First if ends
            }
        }

        public void AddQuestionToTest(int testId, int questionId)
        {
            TestQuestionController tqc = new TestQuestionController();
            tqc.InsertTestQuestion(testId, questionId);
        }

        public void DeleteTest(int testId)
        {
            Test test = GetTest(testId);
            if (test != null)
            {
                using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
                {
                    try
                    {
                        //TO DO the questions from TestQuestion must be deleted.
                        var theTest = db.Tests.SingleOrDefault(t => t.TestId == testId);
                        if (theTest != null)
                        {
                            db.Tests.DeleteOnSubmit(theTest);
                            db.SubmitChanges();
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public Test GetTest(int testId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Tests.SingleOrDefault(t => t.TestId == testId);
                }
                catch (Exception)
                {
                    throw;
                }

            }
        }

        public List<Test> GetAllTests()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Tests.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public List<String> GetAllTestsForClass(int schoolClassId)
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    List<String> theList = new List<String>();
                    var list = db.Teaches.Where(t => t.SchoolClassId == schoolClassId);
                    foreach (Teach item in list)
                    {
                        var anotherList = db.ClassTests.Where(cs => cs.TeachesId == item.TeachesId);
                        foreach (ClassTest item2 in anotherList)
                        {
                            theList.Add(item2.Test.TestName.ToString());
                        }
                    }
                    return theList;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private bool IsNullOrEmpty(string value)
        {
            return (value == null || value == String.Empty);
        }

        public ClassTest SendToClass(Test test, SchoolClass schoolClass, Topic subject, DateTime deadline)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                TeacherController tc = new TeacherController();
                Teacher teacher = tc.GetTeacher(Global.AUTHENTICATED_USER.UserId);
                TeachesController tsc = new TeachesController();
                Teach teach = tsc.GetTeach(teacher.TeacherId, schoolClass.SchoolClassId, subject.TopicId);
                if (teach != null)
                {
                    if (test.TestId > 0 && schoolClass.SchoolClassId > 0)
                    {
                        try
                        {
                            ClassTest ct = new ClassTest();
                            ct.TeachesId = teach.TeachesId;
                            ct.TestId = test.TestId;
                            ct.Deadline = deadline;
                            db.ClassTests.InsertOnSubmit(ct);
                            db.SubmitChanges();
                            return ct;
                        }
                        catch (Exception)
                        {
                            return null;
                        }
                    }
                }
                return null;
            }
        }

        public IList<Test> GetDropDownTests()
        {
            return GetAllTests().OrderByDescending(o => o.TestId).ToList();
        }
        public IList<Test> GetDropDownTests(int teacherId)
        {
            return GetTestsForTeacher(teacherId).OrderByDescending(o => o.TestId).ToList();
        }

        public IList<Question> GetTestQuestionsForDataset(int testId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var lines = db.TestQuestions.Where(tq => tq.TestId == testId);
                return lines.Select(q => q.Question).ToList();

            }
        }

        public IList<QuestionAnswer> GetQuestionAnswerForDataset(int testId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                var rows = (from q in db.Questions
                            join tq in db.TestQuestions on q.QuestionId equals tq.QuestionId
                            join a in db.Answers on tq.TestQuestionId equals a.TestQuestionId into newTable
                            from rowOfNewTable in newTable.DefaultIfEmpty()
                            orderby q.QuestionId
                            where (tq.TestId == testId)
                            select new QuestionAnswer()
                            { //To handle null values do type casting as int?(NULL int) 
                                //since OrderID is defined NOT NULL in tblOrders
                                //OrderID = (int?)rt.OrderID,
                                TestQuestionId = tq.TestQuestionId,
                                QuestionText = q.QuestionText,
                                AnswerText = rowOfNewTable.AnswerText,
                                //AnswerId = (int?)rowOfNewTable.AnswerId,
                                //StudentId = (int?)rowOfNewTable.StudentId
                            }).ToList();
                return rows;
            }
        }

        public IList<Test> GetTestsForClassAndSubject(int schoolClassId, int topicId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<Test> theList = new List<Test>();
                var list = db.Teaches.Where(t => t.TopicId == topicId && t.SchoolClassId == schoolClassId);
                foreach (Teach item in list)
                {
                    var anotherList = db.ClassTests.Where(cs => cs.TeachesId == item.TeachesId);
                    foreach (ClassTest item2 in anotherList)
                    {
                        theList.Add(item2.Test);
                    }
                }
                return theList;
            }
        }
        public IList<Test> GetTestsForClass(int schoolClassId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<Test> theList = new List<Test>();
                var list = db.Teaches.Where(t => t.SchoolClassId == schoolClassId);
                foreach (Teach item in list)
                {
                    var anotherList = db.ClassTests.Where(cs => cs.TeachesId == item.TeachesId);
                    foreach (ClassTest item2 in anotherList)
                    {
                        theList.Add(item2.Test);
                    }
                }
                return theList;
            }
        }
        public IList<Test> GetTestsForTeacher(int teacherId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                IList<Test> theList = new List<Test>();
                return db.Tests.Where(t => t.TeacherId == teacherId || t.TeacherId == null).ToList();
            }
        }
    }
}
