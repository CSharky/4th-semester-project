﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    class TopicController
    {
        public List<Topic> GetAllTopics()
        {
            using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
            {
                try
                {
                    return db.Topics.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public IList<Topic> GetDropDownTopics()
        {
            return GetAllTopics().OrderByDescending(o=> o.TopicId).ToList();
        }
    }
}
