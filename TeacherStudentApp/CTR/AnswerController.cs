﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeacherStudentApp.DAL;

namespace TeacherStudentApp.CTR
{
    class AnswerController
    {
        public Answer GetSingleAnswer(int answerId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    Answer answer = db.Answers.SingleOrDefault(q => q.AnswerId == answerId);
                    return answer;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public Answer GetSingleAnswer(int testQuestionId, int studentId)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                try
                {
                    Answer answer = db.Answers.SingleOrDefault(q => q.TestQuestionId==testQuestionId && q.StudentId==studentId);
                    return answer;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public Answer InsertAnswer(int testQuestionId, int studentId, DateTime answerSent, string answerText)
        {
            using (var db = new TeacherStudentDBDataContext())
            {
                Answer answer = new Answer();
                answer.TestQuestionId = testQuestionId;
                answer.StudentId = studentId;
                answer.AnswerSent = answerSent;
                answer.AnswerText = answerText;

                try
                {
                    db.Answers.InsertOnSubmit(answer);
                    db.SubmitChanges();
                    return GetSingleAnswer(answer.AnswerId);
                }
                catch(Exception)
                {
                    return null;
                }
            }
        }

        public void DeleteAnswer(int answerId)
        {
            Answer answer = GetSingleAnswer(answerId);
            if (answer != null)
            {
                using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
                {
                    try
                    {
                        var theAnswer = db.Answers.SingleOrDefault(a => a.AnswerId== answerId);
                        if (theAnswer != null)
                        {
                            db.Answers.DeleteOnSubmit(theAnswer);
                            db.SubmitChanges();
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public void DeleteAnswer(int testQuestionId, int studentId)
        {
            Answer answer = GetSingleAnswer(testQuestionId,studentId);
            if (answer != null)
            {
                using (TeacherStudentDBDataContext db = new TeacherStudentDBDataContext())
                {
                    try
                    {
                        var theAnswer = db.Answers.SingleOrDefault(a => a.AnswerId == answer.AnswerId);
                        if (theAnswer != null)
                        {
                            db.Answers.DeleteOnSubmit(theAnswer);
                            db.SubmitChanges();
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

        }

								public IList<StudentAnswer> GetAnswersForDataset(int testId, int questionId)
								{
										using (var db = new TeacherStudentDBDataContext())
										{
												var rows = (from tq in db.TestQuestions
																								join a in db.Answers on tq.TestQuestionId equals a.TestQuestionId
																								join s in db.Students on a.StudentId equals s.StudentId
																								orderby a.AnswerId
																								where (tq.TestId == testId && tq.QuestionId == questionId)
																								select new StudentAnswer()
																								{
																										AnswerId = a.AnswerId,
																										StudentId = s.StudentId,
																										FirstName = s.Fname,
																										LastName = s.Lname,
																										AnswerText = a.AnswerText,
																										IsCorrect = ((a.IsCorrect == null) ? false : (bool)a.IsCorrect),
																										Points = 0
																								}).ToList();
												return rows;
										}
								}

								public void SetIsCorrect(int answerId, bool isCorrect)
								{
										using (var db = new TeacherStudentDBDataContext())
										{
												var query =
																from a in db.Answers
																where a.AnswerId == answerId
																select a;

												foreach (Answer a in query)
												{
														a.IsCorrect = isCorrect;
												}

												try
												{
														db.SubmitChanges();
												}
												catch (Exception)
												{
												}
										}
										#region Doesn't work. TODO Investigate the issue.
										//Answer answer = GetSingleAnswer(answerId);

										//if (answer != null)
										//{
										//		using (var db = new TeacherStudentDBDataContext())
										//		{
										//				answer.IsCorrect = isCorrect;

										//				try
										//				{
										//						db.SubmitChanges();
										//				}
										//				catch (Exception)
										//				{
										//				}
										//		}
										//}
										//else
										//{
										//}
										#endregion
								}
    }
}
