﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherStudentApp.DAL
{
    public partial class Test
    {
        public string DisplayName
        {
            get
            {
                return TestName;
            }
        }
    }

    public partial class SchoolClass
    {
        public string DisplayName
        {
            get
            {
                return SchoolClassName;
            }
        }
    }

    public partial class Topic
    {
        public string DisplayName
        {
            get
            {
                return TopicName;
            }
        }
    }
    public partial class Question
    {
        public string DisplayName
        {
            get
            {
                return QuestionText;
            }
        }
    }
}
