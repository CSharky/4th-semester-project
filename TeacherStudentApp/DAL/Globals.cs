﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherStudentApp.DAL
{
    public static class Global
    {
        public static string USER_RIGHTS_STUDENT = "student";
        public static string USER_RIGHTS_LECTURER = "lecturer";
        public static string USER_RIGHTS_ADMIN = "admin";

        public static User AUTHENTICATED_USER { get; set; }
    }
}
