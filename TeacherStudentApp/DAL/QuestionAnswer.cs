﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherStudentApp.DAL
{
		public class QuestionAnswer
		{
				//public int QuestionId { get; set; }
				public int TestQuestionId { get; set; }
				//public int? AnswerId { get; set; }
				//public int? StudentId { get; set; }
				public string QuestionText { get; set; }
				public string AnswerText { get; set; }
		}
}
