﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherStudentApp.DAL
{
		public class StudentAnswer
		{
				public int AnswerId { get; set; }
				public int StudentId { get; set; }
				public string FirstName { get; set; }
				public string LastName { get; set; }
				public string AnswerText { get; set; }
				public bool IsCorrect { get; set; }
				public int Points { get; set; }
		}
}
